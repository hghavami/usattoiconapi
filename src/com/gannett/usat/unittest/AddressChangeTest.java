package com.gannett.usat.unittest;

import org.joda.time.DateTime;

import com.gannett.usat.iconapi.client.IconAPIContext;
import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsSerializer;
import com.gannett.usat.iconapi.domainbeans.addressChange.AddressChangeResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AddressChangeTest extends BaseTest {

	public static void main(String[] args) {
		AddressChangeTest test = new AddressChangeTest();

		test.setUpAPIContext();

		test.testChangeDeliveryAddress();
		test.testChangeBillingAddress();

	}

	public void testChangeDeliveryAddress() {
		try {
			com.gannett.usat.iconapi.client.SubscriptionAddressChange api = new com.gannett.usat.iconapi.client.SubscriptionAddressChange();

			DateTime effectiveDate = new DateTime();
			String pubCode = "GN";
			String accountNumber = "164905";
			String firmName = null;
			String address1 = "1234 Westgate Rd";
			String address2 = "G-7XXX";
			String city = "Anderson";
			String state = "SC";
			String zip = "29626";
			String homePhone = "540-555-4328";
			String workPhone = null;

			IconAPIContext.setApiGCIUnitNumber("9999");

			AddressChangeResponse response = api.createDeliveryAddressChange(pubCode, accountNumber, effectiveDate, firmName,
					address1, address2, city, state, zip, homePhone, workPhone);

			String json = response.getRawResponse();

			System.out.println("Raw JSON: " + json);

			System.out.println("back to json....");

			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Errors.class, new ErrorsSerializer());

			Gson gson2 = builder.create();

			System.out.println(gson2.toJson(response));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void testChangeBillingAddress() {
		try {
			com.gannett.usat.iconapi.client.SubscriptionAddressChange api = new com.gannett.usat.iconapi.client.SubscriptionAddressChange();

			String pubCode = "GN";
			String accountNumber = "413";
			String firmName = null;
			String address1 = "305 S. Main St.";
			String address2 = "G-7";
			String city = "Greenville";
			String state = "SC";
			String zip = "29602";
			String homePhone = "540-555-4328";
			String workPhone = null;

			AddressChangeResponse response = api.createBillingAddressChange(pubCode, accountNumber, firmName, address1, address2,
					city, state, zip, homePhone, workPhone);

			String json = response.getRawResponse();

			System.out.println("Raw JSON: " + json);

			System.out.println("back to json....");

			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Errors.class, new ErrorsSerializer());

			Gson gson2 = builder.create();

			System.out.println(gson2.toJson(response));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
