package com.gannett.usat.unittest;

import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsSerializer;
import com.gannett.usat.iconapi.domainbeans.changeRate.ChangeRateResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ChangeRateTest extends BaseTest {

	public static void main(String[] args) {

		ChangeRateTest test = new ChangeRateTest();

		test.setUpAPIContext();

		test.testChangeRate();
	}

	protected void testChangeRate() {

		try {
			com.gannett.usat.iconapi.client.ChangeRate sChangeRate = new com.gannett.usat.iconapi.client.ChangeRate();

			ChangeRateResponse response = sChangeRate.createChangeRate("GN", "292218", "20120812", "", "C", "", "");

			String json = response.getRawResponse();

			System.out.println("Raw JSON: " + json);

			System.out.println("back to json....");
			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Errors.class, new ErrorsSerializer());

			Gson gson2 = builder.create();

			System.out.println(gson2.toJson(response));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
