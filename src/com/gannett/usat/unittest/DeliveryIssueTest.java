package com.gannett.usat.unittest;

import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsSerializer;
import com.gannett.usat.iconapi.domainbeans.deliveryIssue.DeliveryIssueResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class DeliveryIssueTest extends BaseTest {

	public static void main(String[] args) {

		DeliveryIssueTest test = new DeliveryIssueTest();

		test.setUpAPIContext();

		test.testCreateDeliveryIssue();
	}

	protected void testCreateDeliveryIssue() {

		try {
			com.gannett.usat.iconapi.client.DeliveryIssue dIssue = new com.gannett.usat.iconapi.client.DeliveryIssue();

			DeliveryIssueResponse response = dIssue.createDeliveryIssue("GN", "413", "20120707", "MN");

			String json = response.getRawResponse();

			System.out.println("Raw JSON: " + json);

			System.out.println("back to json....");

			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Errors.class, new ErrorsSerializer());

			Gson gson2 = builder.create();

			System.out.println(gson2.toJson(response));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
