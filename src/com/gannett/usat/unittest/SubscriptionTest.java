package com.gannett.usat.unittest;

import java.util.Collection;

import com.gannett.usat.iconapi.client.Subscription;
import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsSerializer;
import com.gannett.usat.iconapi.domainbeans.subscriberAccount.SubscriberAccount;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SubscriptionTest extends BaseTest {

	public static void main(String[] args) {

		SubscriptionTest test = new SubscriptionTest();

		test.setUpAPIContext();

		String input = null;

		boolean done = false;

		while (!done) {

			input = test
					.getInput("Enter email address or an account number, or press enter for 'aeast@usatoday.com' [q to quit]: ");

			if (input == null || input.length() == 0) {
				input = "aeast@usatoday.com";
			}

			if (input.indexOf("@") > -1) {
				// email
				test.testSubscriberPull(input);
			} else {
				test.testSubscriberPullByAccount(input);
			}
		}

	}

	public void testSubscriberPull(String email) {

		Subscription sApi = new Subscription();

		try {
			String responseJson = sApi.getAccountDataForEmail(email);

			System.out.println(responseJson);

			// convert raw json to objects (could have just called the helper method tht returns objects instead of json
			Collection<SubscriberAccount> accounts = Subscription.jsonToSubscriberAccount(responseJson);

			for (SubscriberAccount accountBean : accounts) {
				System.out.println("Account: " + accountBean.toString());
			}

			System.out.println("back to json....");

			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Errors.class, new ErrorsSerializer());

			Gson gson2 = builder.create();

			System.out.println(gson2.toJson(accounts));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void testSubscriberPullByAccount(String account) {

		Subscription sApi = new Subscription();

		try {
			String responseJson = sApi.getAccountDataForAccountNumber(account, "UT");

			System.out.println(responseJson);

			// convert raw json to objects (could have just called the helper method tht returns objects instead of json
			Collection<SubscriberAccount> accounts = Subscription.jsonToSubscriberAccount(responseJson);

			for (SubscriberAccount accountBean : accounts) {
				System.out.println("Account: " + accountBean.toString());
			}

			System.out.println("back to json....");

			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Errors.class, new ErrorsSerializer());

			Gson gson2 = builder.create();

			System.out.println(gson2.toJson(accounts));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
