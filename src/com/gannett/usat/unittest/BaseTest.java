package com.gannett.usat.unittest;

import java.io.Console;
import java.io.InputStreamReader;

import com.gannett.usat.iconapi.client.IconAPIContext;

public class BaseTest {

	public String getInput(String prompt) {
		String input = null;

		if (prompt == null || prompt.length() == 0) {
			prompt = "Enter input [q to quit]:";
		}

		Console c = System.console();
		if (c == null) {

			InputStreamReader cin = new InputStreamReader(System.in);

			System.out.println(prompt);

			try {
				char[] cbuf = new char[256];
				cin.read(cbuf);

				String tempinput = new String(cbuf);
				if (tempinput != null && tempinput.trim().length() > 0) {
					input = tempinput.trim();
				}

			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		} else {
			input = c.readLine(prompt);
		}

		if (input == null || input.trim().length() == 0) {
			;
		} else if (input.trim().equalsIgnoreCase("q")) {
			System.out.println("Exiting Normally.");
			System.exit(0);
		} else {
			input = input.trim();
		}

		return input;
	}

	public void setUpAPIContext() {
		IconAPIContext.setEndPointURL("http://devsubscriptions.nmass.gci");
		IconAPIContext.setEndPointSecureURL("https://devsubscriptions.nmass.gci");
		IconAPIContext.setEndPointSelfServeSecureURL("http://devsubscription_self_serve.nmass.gci");
		IconAPIContext.setEndPointSelfServeSecureURLPath("/usat_start_subscription");
		IconAPIContext.setEndPointSelfServeSecureURLMakeAPaymentPath("/usat_make_a_payment");
		IconAPIContext.setEndPointSelfServeSecureURLSignUpEzpayPath("/usat_signup_ezpay");
		IconAPIContext.setEndPointSelfServeSecureURLChangeCreditCardPath("//usat_change_credit_card");
		IconAPIContext.setEndPointAddressURL("http://devaddress.nmass.gci");
		IconAPIContext.setEndPointSecureAddressURL("https://devaddress.nmass.gci");
		IconAPIContext.setApiUserID("usat-user");
		IconAPIContext.setApiUserPwd("xyz");
		IconAPIContext.setApiGCIUnitNumber("8872");
		IconAPIContext.setDebugMode(true);
	}
}
