package com.gannett.usat.unittest;

import com.gannett.usat.iconapi.client.IconAPIContext;
import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsSerializer;
import com.gannett.usat.iconapi.domainbeans.nameChange.NameChangeResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class NameChangeTest extends BaseTest {

	public static void main(String[] args) {

		NameChangeTest test = new NameChangeTest();

		test.setUpAPIContext();

		test.testCreateChangeRate();
	}

	protected void testCreateChangeRate() {

		try {
			com.gannett.usat.iconapi.client.NameChange nChange = new com.gannett.usat.iconapi.client.NameChange();

			IconAPIContext.setApiGCIUnitNumber("9999");

			// currently no ssl support
			IconAPIContext.setEndPointSecureURL(IconAPIContext.getEndPointURL());

			NameChangeResponse response = nChange.createNameChange("GN", "164905", "D", "John", "DoeKiller");

			String json = response.getRawResponse();

			System.out.println("Raw JSON: " + json);

			System.out.println("back to json....");

			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Errors.class, new ErrorsSerializer());

			Gson gson2 = builder.create();

			System.out.println(gson2.toJson(response));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
