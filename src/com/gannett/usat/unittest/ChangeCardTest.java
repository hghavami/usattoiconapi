package com.gannett.usat.unittest;

import com.gannett.usat.iconapi.client.IconAPIContext;
import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsSerializer;
import com.gannett.usat.iconapi.domainbeans.changeCard.ChangeCardResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ChangeCardTest extends BaseTest {

	public static void main(String[] args) {

		ChangeCardTest test = new ChangeCardTest();

		test.setUpAPIContext();

		test.testCreateChangeRate();
	}

	protected void testCreateChangeRate() {

		try {
			com.gannett.usat.iconapi.client.ChangeCard cCard = new com.gannett.usat.iconapi.client.ChangeCard();

			IconAPIContext.setApiGCIUnitNumber("9999");

			// currently no ssl support
			IconAPIContext.setEndPointSecureURL(IconAPIContext.getEndPointURL());

			String cardNumber = "5442981111111049";
			// String cardNumber = "4111111111111111";

			// ChangeRateResponse response = sResume.createChangeRate("GN", "413","20120812","","C");

			ChangeCardResponse response = cCard.createChangeCard("GN", "292218", cardNumber, "M", "10/14", "NEW");

			String json = response.getRawResponse();

			System.out.println("Raw JSON: " + json);

			System.out.println("back to json....");

			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Errors.class, new ErrorsSerializer());

			Gson gson2 = builder.create();

			System.out.println(gson2.toJson(response));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
