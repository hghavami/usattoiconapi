package com.gannett.usat.unittest;

import com.gannett.usat.iconapi.client.IconAPIContext;
import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsSerializer;
import com.gannett.usat.iconapi.domainbeans.makePayment.MakeAPaymentResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class MakeAPaymentTest extends BaseTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		MakeAPaymentTest payment = new MakeAPaymentTest();

		payment.setUpAPIContext();

		payment.makeAPayment();

	}

	public void makeAPayment() {
		try {
			com.gannett.usat.iconapi.client.MakePayment payment = new com.gannett.usat.iconapi.client.MakePayment();

			IconAPIContext.setApiGCIUnitNumber("9999");

			// currently no ssl support
			IconAPIContext.setEndPointSecureURL(IconAPIContext.getEndPointURL());

			String cardNumber = "4012888888881881";
			// String cardNumber = "5442981111111049";
			// String cardNumber = "4111111111111111";
			MakeAPaymentResponse response = payment.makePayment("GN", "292218", cardNumber, "V", "10/14", "10.0", "AY", "Y", "0",
					"M");

			String json = response.getRawResponse();

			System.out.println("Raw JSON: " + json);

			System.out.println("back to json....");

			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Errors.class, new ErrorsSerializer());

			Gson gson2 = builder.create();

			System.out.println(gson2.toJson(response));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
