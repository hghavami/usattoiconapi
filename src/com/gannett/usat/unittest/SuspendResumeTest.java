package com.gannett.usat.unittest;

import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsSerializer;
import com.gannett.usat.iconapi.domainbeans.suspendResume.SuspendResumeResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SuspendResumeTest extends BaseTest {

	public static void main(String[] args) {

		SuspendResumeTest test = new SuspendResumeTest();

		test.setUpAPIContext();

		test.testCreateSuspendResume();
	}

	protected void testCreateSuspendResume() {

		try {
			com.gannett.usat.iconapi.client.SuspendResume sResume = new com.gannett.usat.iconapi.client.SuspendResume();

			// SuspendResumeResponse response = sResume.createSuspendResume("GN", "413","20120812","","C");

			SuspendResumeResponse response = sResume.createSuspendResume("UT", "7389728", "20120910", "20120917", "C");

			String json = response.getRawResponse();

			System.out.println("Raw JSON: " + json);

			System.out.println("back to json....");

			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Errors.class, new ErrorsSerializer());

			Gson gson2 = builder.create();

			System.out.println(gson2.toJson(response));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
