package com.gannett.usat.unittest;

import com.gannett.usat.iconapi.client.IconAPIContext;
import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsSerializer;
import com.gannett.usat.iconapi.domainbeans.updateEmail.UpdateEmailResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class UpdateEmailTest extends BaseTest {

	public static void main(String[] args) {

		UpdateEmailTest test = new UpdateEmailTest();

		test.setUpAPIContext();

		test.testCreateUpdateEmail();
	}

	protected void testCreateUpdateEmail() {

		try {
			com.gannett.usat.iconapi.client.UpdateEmail uEmail = new com.gannett.usat.iconapi.client.UpdateEmail();

			IconAPIContext.setApiGCIUnitNumber("8872");

			UpdateEmailResponse response = uEmail.createUpdateEmail("UT", "6039343", "jrmoore@gannett.com", "Y");

			String json = response.getRawResponse();

			System.out.println("Raw JSON: " + json);

			System.out.println("back to json....");

			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Errors.class, new ErrorsSerializer());

			Gson gson2 = builder.create();

			System.out.println(gson2.toJson(response));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
