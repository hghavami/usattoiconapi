package com.gannett.usat.unittest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.gannett.usat.iconapi.client.IconAPIContext;

public class TestHttpsConnection {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String url = "https://devaddress.nmass.gci/address_validations/new/?gci_unit=8872&pub_code=UT&address_line=1201+WALNUT+ST+STE+1101&city_state=KANSAS+CITY+MO64106";
		boolean authenticateRequest = false;
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
		}

		HttpClient httpClient = new DefaultHttpClient();
		String responseJSON = null;
		try {
			System.out.println("URL: " + url);
			HttpRequestBase httpMethod = null;
			httpMethod = new HttpGet(url);
			if (authenticateRequest) {
				String uidPwd = IconAPIContext.getApiUserID() + ":" + IconAPIContext.getApiUserPwd();
				String uidPwdEncoded = new String(Base64.encodeBase64(uidPwd.getBytes()));
				httpMethod.setHeader("Authorization", "Basic " + uidPwdEncoded);

				// original
				httpMethod.setHeader("user", IconAPIContext.getApiUserID() + ":" + IconAPIContext.getApiUserPwd());
			}

			HttpResponse response = httpClient.execute(httpMethod);

			HttpEntity entity = response.getEntity();
			if (entity != null) {
				InputStream is = entity.getContent();
				try {
					StringBuilder sb = new StringBuilder();
					BufferedReader rd = new BufferedReader(new InputStreamReader(is));
					String line;
					while ((line = rd.readLine()) != null) {
						sb.append(line);
					}
					rd.close();
					responseJSON = sb.toString();
					// do something useful with the response
				} catch (IOException ex) {
					// In case of an IOException the connection will be released
					// back to the connection manager automatically
					throw ex;
				} catch (RuntimeException ex) {
					// In case of an unexpected exception you may want to abort
					// the HTTP request in order to shut down the underlying
					// connection immediately.
					httpMethod.abort();
					throw ex;
				} finally {
					// Closing the input stream will trigger connection release
					try {
						is.close();
					} catch (Exception ignore) {
					}
				}
			}

			EntityUtils.consume(entity);

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			// When HttpClient instance is no longer needed,
			// shut down the connection manager to ensure
			// immediate deallocation of all system resources
			httpClient.getConnectionManager().shutdown();
		}
		System.out.println("ICON API Response: " + responseJSON);
	}

}
