package com.gannett.usat.iconapi.client;

public class IconAPIContext {

	protected static String endPointURL = null;
	// protected static String endPointSecretHash = null;
	protected static String endPointSecureURL = null;
	protected static String endPointSelfServeSecureURL = null;
	protected static String endPointSelfServeSecureURLPath = null;
	protected static String endPointSelfServeSecureURLMakeAPaymentPath = null;
	protected static String endPointSelfServeSecureURLSignUpEzpayPath = null;
	protected static String endPointSelfServeSecureURLChangeCreditCardPath = null;
	
	protected static String endPointAddressURL = null;
	// protected static String endPointSecretHash = null;
	protected static String endPointSecureAddressURL = null;
	protected static String apiUserID = "usat-user";
	protected static String apiUserPwd = "xyz";
	protected static String apiGCIUnitNumber = "9999";

	public static boolean debugMode = false;

	public static String getEndPointURL() {
		return endPointURL;
	}

	public static void setEndPointURL(String endPointURL) {
		IconAPIContext.endPointURL = endPointURL;
	}

	/*
	 * public static String getEndPointSecretHash() { return endPointSecretHash; } public static void setEndPointSecretHash(String
	 * endPointSecretHash) { IconAPIContext.endPointSecretHash = endPointSecretHash; }
	 */
	public static String getApiUserID() {
		return apiUserID;
	}

	public static void setApiUserID(String apiUserID) {
		IconAPIContext.apiUserID = apiUserID;
	}

	public static String getApiUserPwd() {
		return apiUserPwd;
	}

	public static void setApiUserPwd(String apiUserPwd) {
		IconAPIContext.apiUserPwd = apiUserPwd;
	}

	public static String getApiGCIUnitNumber() {
		return apiGCIUnitNumber;
	}

	public static void setApiGCIUnitNumber(String apiGCIUnitNumber) {
		IconAPIContext.apiGCIUnitNumber = apiGCIUnitNumber;
	}

	public static boolean isDebugMode() {
		return debugMode;
	}

	public static void setDebugMode(boolean debugMode) {
		IconAPIContext.debugMode = debugMode;
	}

	public static String getEndPointSecureURL() {
		return endPointSecureURL;
	}

	public static void setEndPointSecureURL(String endPointSecureURL) {
		IconAPIContext.endPointSecureURL = endPointSecureURL;
	}

	public static String getEndPointAddressURL() {
		return endPointAddressURL;
	}

	public static void setEndPointAddressURL(String endPointAddressURL) {
		IconAPIContext.endPointAddressURL = endPointAddressURL;
	}

	public static String getEndPointSecureAddressURL() {
		return endPointSecureAddressURL;
	}

	public static void setEndPointSecureAddressURL(String endPointSecureAddressURL) {
		IconAPIContext.endPointSecureAddressURL = endPointSecureAddressURL;
	}

	public static String getEndPointSelfServeSecureURL() {
		return endPointSelfServeSecureURL;
	}

	public static void setEndPointSelfServeSecureURL(String endPointSelfServeSecureURL) {
		IconAPIContext.endPointSelfServeSecureURL = endPointSelfServeSecureURL;
	}

	public static String getEndPointSelfServeSecureURLPath() {
		return endPointSelfServeSecureURLPath;
	}

	public static void setEndPointSelfServeSecureURLPath(String endPointSelfServeSecureURLPath) {
		IconAPIContext.endPointSelfServeSecureURLPath = endPointSelfServeSecureURLPath;
	}

	public static String getEndPointSelfServeSecureURLMakeAPaymentPath() {
		return endPointSelfServeSecureURLMakeAPaymentPath;
	}

	public static void setEndPointSelfServeSecureURLMakeAPaymentPath(String endPointSelfServeSecureURLMakeAPaymentPath) {
		IconAPIContext.endPointSelfServeSecureURLMakeAPaymentPath = endPointSelfServeSecureURLMakeAPaymentPath;
	}

	public static String getEndPointSelfServeSecureURLChangeCreditCardPath() {
		return endPointSelfServeSecureURLChangeCreditCardPath;
	}

	public static void setEndPointSelfServeSecureURLChangeCreditCardPath(String endPointSelfServeSecureURLChangeCreditCardPath) {
		IconAPIContext.endPointSelfServeSecureURLChangeCreditCardPath = endPointSelfServeSecureURLChangeCreditCardPath;
	}

	public static String getEndPointSelfServeSecureURLSignUpEzpayPath() {
		return endPointSelfServeSecureURLSignUpEzpayPath;
	}

	public static void setEndPointSelfServeSecureURLSignUpEzpayPath(String endPointSelfServeSecureURLSignUpEzpayPath) {
		IconAPIContext.endPointSelfServeSecureURLSignUpEzpayPath = endPointSelfServeSecureURLSignUpEzpayPath;
	}
}