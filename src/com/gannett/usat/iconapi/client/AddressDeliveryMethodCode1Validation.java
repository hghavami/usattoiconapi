package com.gannett.usat.iconapi.client;

import java.net.URL;
import java.net.URLEncoder;

import org.apache.http.client.utils.URIBuilder;

import com.gannett.usat.iconapi.domainbeans.Code1Validation.Code1ValidationWrapper;
import com.gannett.usat.iconapi.domainbeans.DeliveryMethod.DeliveryMethodCode1ValidationWrapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AddressDeliveryMethodCode1Validation extends IconAPIBase {

	public AddressDeliveryMethodCode1Validation() {
		super();

	}

	public DeliveryMethodCode1ValidationWrapper getAddressDeliveryMethodCode1ValidateObject(String pubCode, String street1,
			String street2, String city, String state, String zip) throws Exception {

		String json = this.getAddressDeliveryMethodCode1ValidateData(pubCode, street1, street2, city, state, zip);
		return AddressDeliveryMethodCode1Validation.jsonToDeliveryMethodCode1Validation(json);
	}

	/**
	 * 
	 * @param emailAddress
	 *            - The email address for whom we want accounts.
	 * @return Raw JSON response as a String object
	 * @throws Exception
	 */
	public String getAddressDeliveryMethodCode1ValidateData(String pubCode, String street1, String street2, String city,
			String state, String zip) throws Exception {
		String responseJSON = null;

		URL url = new URL(this.getBaseAPIAddressURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(ADDRESS_DELMETH_CODE1_PATH);
		builder.addParameter("gci_unit", IconAPIContext.getApiGCIUnitNumber());
		if (pubCode != null && !pubCode.trim().equals("")) {
			builder.addParameter("pub_code", pubCode);
		} else {
			builder.addParameter("pub_code", "");
		}

		if (street1 != null && !street1.equals("")) {
			builder.addParameter("address_line", street1.trim() + " " + street2.trim());
		} else {
			builder.addParameter("address_line", "");
		}

		if (city != null && !city.equals("")) {
			builder.addParameter("city_state", city.trim() + " " + state.trim());
		} else {
			builder.addParameter("city_state", "");
		}

		if (zip != null && !zip.equals("")) {
			builder.addParameter("zip_code", zip);
		} else {
			builder.addParameter("zip_code", "");
		}

		builder.addParameter("router_search", "");

		// build up URL
		java.net.URI uri = builder.build();

		// for some reason when we use setParameter above for email it double encodes the '@' in the email address.
		String uriString = uri.toString();
		uriString = uriString + URLEncoder.encode("B", "UTF-8");

		responseJSON = this.makeAPIGetRequest(uriString, false);

		return responseJSON;

	}

	public Code1ValidationWrapper getAddressCode1ValidationObject(String pubCode, String street1, String street2, String city,
			String state, String zip) throws Exception {

		String json = this.getAddressCode1ValidateData(pubCode, street1, street2, city, state, zip);
		return AddressDeliveryMethodCode1Validation.jsonToCode1Validation(json);
	}

	/**
	 * 
	 * @param emailAddress
	 *            - The email address for whom we want accounts.
	 * @return Raw JSON response as a String object
	 * @throws Exception
	 */
	public String getAddressCode1ValidateData(String pubCode, String street1, String street2, String city, String state, String zip)
			throws Exception {
		String responseJSON = null;

		URL url = new URL(this.getBaseAPIAddressURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(ADDRESS_DELMETH_CODE1_PATH);
		builder.addParameter("gci_unit", IconAPIContext.getApiGCIUnitNumber());
		if (pubCode != null && !pubCode.trim().equals("")) {
			builder.addParameter("pub_code", pubCode);
		} else {
			builder.addParameter("pub_code", "");
		}

		if (street1 != null && !street1.equals("")) {
			builder.addParameter("address_line", street1.trim() + " " + street2.trim());
		} else {
			builder.addParameter("address_line", "");
		}

		if (city != null && !city.equals("")) {
			builder.addParameter("city_state", city.trim() + " " + state.trim());
		} else {
			builder.addParameter("city_state", "");
		}

		// build up URL
		java.net.URI uri = builder.build();

		// for some reason when we use setParameter above for email it double encodes the '@' in the email address.
		String uriString = uri.toString();
		if (zip != null && !zip.equals("")) {
			uriString = uriString + URLEncoder.encode(zip, "UTF-8");
		} else {
			uriString = uriString + URLEncoder.encode("", "UTF-8");
		}

		responseJSON = this.makeAPIGetRequest(uriString, false);

		return responseJSON;

	}

	/**
	 * Converts json returned from the ICON Subscription API to Objects
	 * 
	 * @param json
	 *            - A json string as returned from the API
	 * @return Collection of SubscriberAccount objects
	 */
	public static DeliveryMethodCode1ValidationWrapper jsonToDeliveryMethodCode1Validation(String json) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();

		// Type collectionType = new TypeToken<Collection<DeliveryMethodCode1ValidationWrapper>>(){}.getType();
		// DeliveryMethodCode1ValidationWrapper dmcv = gson.fromJson(json, collectionType);
		DeliveryMethodCode1ValidationWrapper dmcv = gson.fromJson(json, DeliveryMethodCode1ValidationWrapper.class);
		return dmcv;
	}

	/**
	 * Converts json returned from the ICON Subscription API to Objects
	 * 
	 * @param json
	 *            - A json string as returned from the API
	 * @return Collection of SubscriberAccount objects
	 */
	public static Code1ValidationWrapper jsonToCode1Validation(String json) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();

		// Type collectionType = new TypeToken<Collection<Code1ValidationWrapper>>(){}.getType();
		// Code1ValidationWrapper dmcv = gson.fromJson(json, collectionType);
		Code1ValidationWrapper dmcv = gson.fromJson(json, Code1ValidationWrapper.class);

		return dmcv;
	}

}
