package com.gannett.usat.iconapi.client;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsDeserializer;
import com.gannett.usat.iconapi.domainbeans.deliveryIssue.DeliveryIssueResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * This class is used to communicate with the ICON API for reporting delivery issues/complaints
 * 
 * @author aeast
 * 
 */
public class DeliveryIssue extends IconAPIBase {

	/**
	 * Converts json returned from the ICON Subscription API to Objects
	 * 
	 * @param json
	 *            - A json string as returned from the API
	 * @return Collection of SubscriberAccount objects
	 */
	public static DeliveryIssueResponse jsonToDeliveryIssueResponse(String json) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Errors.class, new ErrorsDeserializer());
		Gson gson = gsonBuilder.create();

		DeliveryIssueResponse response = null;

		Type delIssueResponseType = new TypeToken<DeliveryIssueResponse>() {
		}.getType();

		try {
			response = gson.fromJson(json, delIssueResponseType);
		} catch (Exception e) {
			e.printStackTrace();
			if (response == null) {
				response = new DeliveryIssueResponse();
			}
			response.setRawResponse(json);
		}

		return response;
	}

	/**
	 * 
	 * @param pubCode
	 * @param accountNumber
	 * @param effectiveDate
	 *            format should be in YYYYMMDD
	 * @param messageCode
	 *            The code from MSGTAB table
	 * @return
	 * @throws Exception
	 */
	public DeliveryIssueResponse createDeliveryIssue(String pubCode, String accountNumber, String effectiveDate, String messageCode)
			throws Exception {

		String responseJSON = null;

		URL url = new URL(this.getBaseAPIURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(DELIVERY_ISSUE_PATH);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		if (IconAPIContext.getApiGCIUnitNumber() != null) {
			nameValuePairs.add(new BasicNameValuePair("gci_unit", IconAPIContext.getApiGCIUnitNumber()));
		}

		if (pubCode != null) {
			nameValuePairs.add(new BasicNameValuePair("pub_code", pubCode));
		}

		if (accountNumber != null) {
			nameValuePairs.add(new BasicNameValuePair("account_number", accountNumber));
		}

		if (effectiveDate != null) {
			nameValuePairs.add(new BasicNameValuePair("effective_date", effectiveDate));
		}

		if (messageCode != null) {
			nameValuePairs.add(new BasicNameValuePair("message_code", messageCode));
		}

		// Message type always E per documentation
		nameValuePairs.add(new BasicNameValuePair("message_type", "E"));

		// Subscriber Action is alway a 'C' for credit for USAT/BW
		nameValuePairs.add(new BasicNameValuePair("subscriber_action", "C"));
		
		// Check code is always 'Y' for checking the MSGTAB file for correct delivery issue code 
		nameValuePairs.add(new BasicNameValuePair("check_code", "Y"));

		// build up URL
		java.net.URI uri = builder.build();

		String uriString = uri.toString();

		responseJSON = this.makeAPIPostRequest(uriString, nameValuePairs);

		DeliveryIssueResponse response = DeliveryIssue.jsonToDeliveryIssueResponse(responseJSON);

		return response;

	}
}
