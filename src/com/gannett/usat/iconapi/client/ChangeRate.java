package com.gannett.usat.iconapi.client;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsDeserializer;
import com.gannett.usat.iconapi.domainbeans.changeRate.ChangeRateResponse;
import com.gannett.usat.iconapi.domainbeans.suspendResume.SuspendResumeResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * This class is used to communicate with the ICON API for vacation stops/starts
 * 
 * @author aeast
 * 
 */
public class ChangeRate extends IconAPIBase {

	/**
	 * Converts json returned from the ICON Subscription API to Objects
	 * 
	 * @param json
	 *            - A json string as returned from the API
	 * @return Collection of SubscriberAccount objects
	 */
	public static ChangeRateResponse jsonToChangeRateResponse(String json) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Errors.class, new ErrorsDeserializer());
		Gson gson = gsonBuilder.create();

		ChangeRateResponse response = null;

		Type suspendResumeResponseType = new TypeToken<SuspendResumeResponse>() {
		}.getType();

		try {
			response = gson.fromJson(json, suspendResumeResponseType);
		} catch (Exception e) {
			e.printStackTrace();
			if (response == null) {
				response = new ChangeRateResponse();
			}
			response.setRawResponse(json);
		}

		return response;
	}

	/**
	 * 
	 * @param pubCode
	 * @param accountNumber
	 * @param effectiveDate
	 *            format should be in YYYYMMDD
	 * @param messageCode
	 *            The code from MSGTAB table
	 * @return
	 * @throws Exception
	 */
	public ChangeRateResponse createChangeRate(String pubCode, String accountNumber, String rateCode, String effectiveDate,
			String sourceCode, String promoCode, String contestCode) throws Exception {

		String responseJSON = null;

		URL url = new URL(this.getBaseAPIURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(CHANGE_RATE_PATH);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		if (IconAPIContext.getApiGCIUnitNumber() != null) {
			nameValuePairs.add(new BasicNameValuePair("gci_unit", IconAPIContext.getApiGCIUnitNumber()));
		}

		if (pubCode != null) {
			nameValuePairs.add(new BasicNameValuePair("pub_code", pubCode));
		}

		if (accountNumber != null) {
			nameValuePairs.add(new BasicNameValuePair("account_number", accountNumber));
		}

		if (rateCode != null) {
			nameValuePairs.add(new BasicNameValuePair("rate_code", rateCode));
		}

		if (effectiveDate != null) {
			nameValuePairs.add(new BasicNameValuePair("effective_date", effectiveDate));
		}

		if (sourceCode != null) {
			nameValuePairs.add(new BasicNameValuePair("souce_code", sourceCode));
		}

		if (rateCode != null) {
			nameValuePairs.add(new BasicNameValuePair("rate_code", rateCode));
		}

		if (promoCode != null) {
			nameValuePairs.add(new BasicNameValuePair("promo_code", promoCode));
		}

		// build up URL
		java.net.URI uri = builder.build();

		String uriString = uri.toString();

		responseJSON = this.makeAPIPostRequest(uriString, nameValuePairs);

		ChangeRateResponse response = ChangeRate.jsonToChangeRateResponse(responseJSON);

		return response;

	}
}
