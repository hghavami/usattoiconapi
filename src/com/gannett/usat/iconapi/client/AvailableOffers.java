package com.gannett.usat.iconapi.client;

import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Collection;

import org.apache.http.client.utils.URIBuilder;

import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsDeserializer;
import com.gannett.usat.iconapi.domainbeans.ratesTerms.RatesTerms;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class AvailableOffers extends IconAPIBase {

	public AvailableOffers() {
		super();

	}

	/**
	 * Converts json returned from the ICON Subscription API to Objects
	 * 
	 * @param json
	 *            - A json string as returned from the API
	 * @return Collection of SubscriberAccount objects
	 */
	public static Collection<RatesTerms> jsonToRatesTerms(String json) {
		try {
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.registerTypeAdapter(Errors.class, new ErrorsDeserializer());
			Gson gson = gsonBuilder.create();
	
			Type collectionType = new TypeToken<Collection<RatesTerms>>() {
			}.getType();
			Collection<RatesTerms> rates = gson.fromJson(json, collectionType);			
			return rates;
		} catch (Exception e) {
			System.out.println("API response was not JSON and could not be understood because: " + e);
			return null;
		}
	}

	/**
	 * 
	 * @return Raw JSON response as a String object
	 * @throws Exception
	 */
	public String getAvailableOffers(String pubCode, String accountNumber, String sourceCode, String promoCode, String contestCode,
			String fodCode, String renewalCode) throws Exception {
		String responseJSON = null;

		URL url = new URL(this.getBaseAPIURL());

		URIBuilder builder = new URIBuilder();

		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(AVAILABLE_OFFERS);

		// remove hardcode hdcons-153 hghavami
		builder.addParameter("gci_unit", IconAPIContext.getApiGCIUnitNumber());
		// builder.addParameter("gci_unit", IconAPIContext.getApiGCIUnitNumber());
		builder.addParameter("pub_code", pubCode);
		//builder.addParameter("source_code", sourceCode);
		//builder.addParameter("account_number", accountNumber);
		//builder.addParameter("promo_code", promoCode);
		//builder.addParameter("contest_code", contestCode);
		//builder.addParameter("fod", fodCode);
		//builder.addParameter("renewal", renewalCode);

		// Check and build up URL
		java.net.URI uri = builder.build();
		if (sourceCode == null) {
			sourceCode= "";
		}
		if (accountNumber == null) {
			accountNumber = "";
		}
		if (promoCode == null) {
			promoCode = "";
		}
		if (contestCode == null) {
			contestCode = "";
		}
		if (fodCode == null) {
			fodCode = "";
		}
		if (renewalCode == null) {
			renewalCode = "";
		}
		/////////////////////////////// NEW
		StringBuilder uriString = new StringBuilder(uri.toString());
		uriString.append("&source_code=").append(URLEncoder.encode(sourceCode, "UTF-8"));
		uriString.append("&account_number=").append(URLEncoder.encode(accountNumber, "UTF-8"));
		uriString.append("&promo_code=").append(URLEncoder.encode(promoCode, "UTF-8"));
		uriString.append("&contest_code=").append(URLEncoder.encode(contestCode, "UTF-8"));
		uriString.append("&fod=").append(URLEncoder.encode(fodCode, "UTF-8"));
		uriString.append("&renewal=").append(URLEncoder.encode(renewalCode, "UTF-8"));
		//////////////////////////////////
		
		responseJSON = this.makeAPIGetRequest(uriString.toString());

		return responseJSON;

	}

	/**
	 * This method returns objects created from the api response
	 * 
	 * @param emailAddress
	 *            - Used to query Genesys
	 * @return A collection of com SubscriberAccount beans that can be used for setting up business objects or displayed on a screen
	 *         If you already have a json string from calling getAccountDataForEmail, you should use the helper method to convert it
	 *         to objects
	 * @throws Exception
	 */

	// Genesys hdcons-153 hghavami used for testing
	public Collection<RatesTerms> getAvailableOffersObjects(String pubCode, String accountNumber, String sourceCode,
			String promoCode, String contestCode, String fodCode, String renewalCode) throws Exception {

		String json = this.getAvailableOffers(pubCode, accountNumber, sourceCode, promoCode, contestCode, fodCode, renewalCode);

		return AvailableOffers.jsonToRatesTerms(json);
	}

}
