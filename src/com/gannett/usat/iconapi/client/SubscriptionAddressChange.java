package com.gannett.usat.iconapi.client;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.joda.time.DateTime;

import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsDeserializer;
import com.gannett.usat.iconapi.domainbeans.addressChange.AddressChangeResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class SubscriptionAddressChange extends IconAPIBase {
	/**
	 * Converts json returned from the ICON Subscription API to Objects
	 * 
	 * @param json
	 *            - A json string as returned from the API
	 * @return Collection of SubscriberAccount objects
	 */
	public static AddressChangeResponse jsonToAddressChangeResponse(String json) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Errors.class, new ErrorsDeserializer());
		Gson gson = gsonBuilder.create();

		AddressChangeResponse response = null;

		Type responseType = new TypeToken<AddressChangeResponse>() {
		}.getType();

		try {
			response = gson.fromJson(json, responseType);
		} catch (Exception e) {
			e.printStackTrace();
			if (response == null) {
				response = new AddressChangeResponse();
			}
			response.setRawResponse(json);
		}

		return response;
	}

	/**
	 * 
	 * @param pubCode
	 * @param accountNumber
	 * @param effectiveDate
	 *            Format: DateTime object - only date will be used
	 * @param firmName
	 * @param address1
	 *            Address1 + Apt/Suite
	 * @param address2
	 *            Additional Address non routable info
	 * @param city
	 * @param state
	 * @param zip
	 * @param homePhone
	 *            Format: xxx-xxx-xxxx
	 * @param workPhone
	 *            Format: xxx-xxx-xxxx
	 * @return AddressChangeResponse object
	 * @throws Exception
	 */
	public AddressChangeResponse createDeliveryAddressChange(String pubCode, String accountNumber, DateTime effectiveDate,
			String firmName, String address1, String address2, String city, String state, String zip, String homePhone,
			String workPhone) throws Exception {

		return this.createAddressChange(pubCode, accountNumber, effectiveDate, firmName, address1, address2, city, state, zip,
				homePhone, workPhone, "D");

	}

	/**
	 * 
	 * @param pubCode
	 * @param accountNumber
	 * @param firmName
	 * @param address1
	 *            Address1 + Apt/Suite
	 * @param address2
	 *            Additional Address non routable info
	 * @param city
	 * @param state
	 * @param zip
	 * @param homePhone
	 *            Format: xxx-xxx-xxxx
	 * @param workPhone
	 *            Format: xxx-xxx-xxxx
	 * @return AddressChangeResponse object
	 * @throws Exception
	 */
	public AddressChangeResponse createBillingAddressChange(String pubCode, String accountNumber, String firmName, String address1,
			String address2, String city, String state, String zip, String homePhone, String workPhone) throws Exception {

		return this.createAddressChange(pubCode, accountNumber, null, firmName, address1, address2, city, state, zip, homePhone,
				workPhone, "B");

	}

	private AddressChangeResponse createAddressChange(String pubCode, String accountNumber, DateTime effectiveDate,
			String firmName, String address1, String address2, String city, String state, String zip, String homePhone,
			String workPhone, String typeOfAddress) throws Exception {
		String responseJSON = null;

		URL url = new URL(this.getBaseAPIURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(IconAPIBase.ADDRESS_CHANGE_PATH);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		if (IconAPIContext.getApiGCIUnitNumber() != null) {
			nameValuePairs.add(new BasicNameValuePair("gci_unit", IconAPIContext.getApiGCIUnitNumber()));
		}

		if (pubCode != null) {
			nameValuePairs.add(new BasicNameValuePair("pub_code", pubCode));
		}

		if (accountNumber != null) {
			nameValuePairs.add(new BasicNameValuePair("account_number", accountNumber));
		}

		if (effectiveDate != null) {
			nameValuePairs.add(new BasicNameValuePair("effective_date", effectiveDate.toString("yyyyMMdd")));
		}

		if (typeOfAddress != null) {
			nameValuePairs.add(new BasicNameValuePair("address_type", typeOfAddress));
		}

		if (firmName != null) {
//			nameValuePairs.add(new BasicNameValuePair("firm_name", firmName));
			nameValuePairs.add(new BasicNameValuePair("address_line_2", firmName));			
		}

		if (address1 != null) {
			nameValuePairs.add(new BasicNameValuePair("address_line_1", address1));
		}

		if (address2 != null) {
//			nameValuePairs.add(new BasicNameValuePair("address_line_2", address2));
			nameValuePairs.add(new BasicNameValuePair("additional_delivery_info", address2));			
		}

		if (city != null) {
			nameValuePairs.add(new BasicNameValuePair("city", city));
		}

		if (state != null) {
			nameValuePairs.add(new BasicNameValuePair("state", state));
		}

		if (zip != null) {
			nameValuePairs.add(new BasicNameValuePair("zip_code", zip));
		}

		if (homePhone != null) {
			nameValuePairs.add(new BasicNameValuePair("home_phone", homePhone));
		}

		if (workPhone != null) {
			nameValuePairs.add(new BasicNameValuePair("work_phone", workPhone));
		}

		// build up URL
		java.net.URI uri = builder.build();

		String uriString = uri.toString();

		responseJSON = this.makeAPIPostRequest(uriString, nameValuePairs);

		AddressChangeResponse response = SubscriptionAddressChange.jsonToAddressChangeResponse(responseJSON);

		return response;

	}
}
