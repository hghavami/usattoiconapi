package com.gannett.usat.iconapi.client;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsDeserializer;
import com.gannett.usat.iconapi.domainbeans.makePayment.MakeAPaymentResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class MakePayment extends IconAPIBase {

	/**
	 * Converts json returned from the ICON Make A Payment API to Objects
	 * 
	 * @param json
	 *            - A json string as returned from the API
	 * @return Make a Payment Response
	 */
	public static MakeAPaymentResponse jsonToMakePaymentResponse(String json) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Errors.class, new ErrorsDeserializer());
		Gson gson = gsonBuilder.create();

		MakeAPaymentResponse response = null;

		Type responseType = new TypeToken<MakeAPaymentResponse>() {
		}.getType();

		try {
			response = gson.fromJson(json, responseType);
		} catch (Exception e) {
			e.printStackTrace();
			if (response == null) {
				response = new MakeAPaymentResponse();
			}
			response.setRawResponse(json);
		}

		return response;
	}

	/**
	 * 
	 * @param pubCode
	 * @param accountNumber
	 * @param cardNumber
	 *            The credit card number as entered by customer
	 * @param cardType
	 * @param cardExpiration
	 * @param paymentAmount
	 * @param rateCode
	 * @return
	 * @throws Exception
	 */
	public MakeAPaymentResponse makePayment(String pubCode, String accountNumber, String cardNumber, String cardType,
			String cardExpiration, String paymentAmount, String rateCode, String enrollInEZPAY, String termlength,
			String durationType) throws Exception {

		String responseJSON = null;

//		URL url = new URL(this.getSecureBaseAPIURL());
		URL url = new URL(this.getSecureSelfServeAPIURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(MAKE_PAYMENT_PATH);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		if (IconAPIContext.getApiGCIUnitNumber() != null) {
			nameValuePairs.add(new BasicNameValuePair("gci_unit", IconAPIContext.getApiGCIUnitNumber()));
		}

		if (pubCode != null) {
			nameValuePairs.add(new BasicNameValuePair("pub_code", pubCode));
		}

		if (accountNumber != null) {
			nameValuePairs.add(new BasicNameValuePair("account_number", accountNumber));
		}

		// String encodedNumber = new String(Base64.encodeBase64(cardNumber.getBytes()));

		if (cardNumber != null) {
			nameValuePairs.add(new BasicNameValuePair("card_number", cardNumber));
		}

		if (cardType != null) {
			nameValuePairs.add(new BasicNameValuePair("card_type", cardType));
		}

		if (cardExpiration != null) {
			nameValuePairs.add(new BasicNameValuePair("card_exp", cardExpiration));
		}

		if (paymentAmount != null) {
			nameValuePairs.add(new BasicNameValuePair("total_payment", paymentAmount));
		}

		if (rateCode != null) {
			nameValuePairs.add(new BasicNameValuePair("rate_code", rateCode));
		}

		if (enrollInEZPAY != null) {
			nameValuePairs.add(new BasicNameValuePair("enroll_ezpay", enrollInEZPAY));
		}

		// nameValuePairs.add(new BasicNameValuePair("enroll_paperless_billing", "N"));

		if (termlength != null) {
			nameValuePairs.add(new BasicNameValuePair("term_length", termlength));
		}

		if (durationType != null) {
			nameValuePairs.add(new BasicNameValuePair("rate_period", durationType));
		}

		// build up URL
		java.net.URI uri = builder.build();

		String uriString = uri.toString();

		responseJSON = this.makeAPIPostRequest(uriString, nameValuePairs);

		MakeAPaymentResponse response = MakePayment.jsonToMakePaymentResponse(responseJSON);

		return response;

	}

}
