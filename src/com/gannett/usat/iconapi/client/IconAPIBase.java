package com.gannett.usat.iconapi.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public abstract class IconAPIBase {

	public static final String SUBSCRIPTIONS_PATH = "/subscription_details";
	public static final String DELIVERY_ISSUE_PATH = "/delivery_service_issue";
	public static final String ADDRESS_CHANGE_PATH = "/change_of_subscription";
	public static final String SUSPEND_RESUME_PATH = "/suspend_resume";
	public static final String CHANGE_RATE_PATH = "/change_rate";
//	public static final String MAKE_PAYMENT_PATH = "/make_a_payment";
	public static final String MAKE_PAYMENT_PATH = IconAPIContext.getEndPointSelfServeSecureURLMakeAPaymentPath();
//	public static final String CHANGE_CARD_PATH = "/change_credit_card";
	public static final String CHANGE_CARD_PATH = IconAPIContext.getEndPointSelfServeSecureURLChangeCreditCardPath();
//	public static final String CHANGE_CARD__EZPAY_PATH = "/signup_ezpay";
	public static final String CHANGE_CARD_EZPAY_PATH = IconAPIContext.getEndPointSelfServeSecureURLSignUpEzpayPath();
	public static final String ADDRESS_DELMETH_CODE1_PATH = "/address_validations/new/";
//	public static final String NEW_SUBSCRIPTION_START_PATH = "/start_subscription";
	public static final String NEW_SUBSCRIPTION_START_PATH = IconAPIContext.getEndPointSelfServeSecureURLPath();
	public static final String RATES_AND_TERMS = "/rates_and_terms";
	public static final String AVAILABLE_OFFERS = "/available_offers";
	public static final String DISCLAIMERS = "/disclaimer/new";

	/**
	 * 
	 * @return The base url ending in '/'
	 * 
	 */
	protected String getBaseAPIURL() {
		StringBuilder sb = new StringBuilder(IconAPIContext.getEndPointURL());

		// the endpoint should end with a '/' so add it if it doesn't have it
		if (sb.charAt(sb.length() - 1) != '/') {
			sb.append("/");
		}

		return sb.toString();
	}

	protected String getSecureBaseAPIURL() {
		StringBuilder sb = new StringBuilder(IconAPIContext.getEndPointSecureURL());

		// the endpoint should end with a '/' so add it if it doesn't have it
		if (sb.charAt(sb.length() - 1) != '/') {
			sb.append("/");
		}

		return sb.toString();
	}

	/**
	 * 
	 * @param url
	 *            The fully qualified REST request - GET
	 * @return The raw JSON response
	 * @throws Exception
	 */
	protected String makeAPIGetRequest(String url, boolean authenticateRequest) throws Exception {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		try {
//			SSLContext sc = SSLContext.getInstance("SSL");
			SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
		}

		HttpClient httpClient = new DefaultHttpClient();
		String responseJSON = null;
		try {

			if (IconAPIContext.debugMode) {
				System.out.println("ICON API GET Method: " + url);
			}

			HttpRequestBase httpMethod = null;
			httpMethod = new HttpGet(url);
			if (authenticateRequest) {
				String uidPwd = IconAPIContext.getApiUserID() + ":" + IconAPIContext.getApiUserPwd();
				String uidPwdEncoded = new String(Base64.encodeBase64(uidPwd.getBytes()));
				httpMethod.setHeader("Authorization", "Basic " + uidPwdEncoded);
				
				// original
				httpMethod.setHeader("user", IconAPIContext.getApiUserID() + ":" + IconAPIContext.getApiUserPwd());
			}

			HttpResponse response = httpClient.execute(httpMethod);

			HttpEntity entity = response.getEntity();
			if (entity != null) {
				InputStream is = entity.getContent();
				try {
					StringBuilder sb = new StringBuilder();
					BufferedReader rd = new BufferedReader(new InputStreamReader(is));
					String line;
					while ((line = rd.readLine()) != null) {
						sb.append(line);
					}
					rd.close();
					responseJSON = sb.toString();
					// do something useful with the response
				} catch (IOException ex) {
					// In case of an IOException the connection will be released
					// back to the connection manager automatically
					throw ex;
				} catch (RuntimeException ex) {
					// In case of an unexpected exception you may want to abort
					// the HTTP request in order to shut down the underlying
					// connection immediately.
					httpMethod.abort();
					throw ex;
				} finally {
					// Closing the input stream will trigger connection release
					try {
						is.close();
					} catch (Exception ignore) {
					}
				}
			}

			EntityUtils.consume(entity);

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			// When HttpClient instance is no longer needed,
			// shut down the connection manager to ensure
			// immediate deallocation of all system resources
			httpClient.getConnectionManager().shutdown();

		}
		if (IconAPIContext.debugMode) {
			System.out.println("ICON API Response: " + responseJSON);
		}

		return responseJSON;
	}

	/**
	 * 
	 * @param url
	 * @param formValues
	 * @return
	 * @throws Exception
	 */
	protected String makeAPIPostRequest(String url, List<NameValuePair> formValues) throws Exception {
		HttpClient httpClient = new DefaultHttpClient();
		String responseJSON = null;

		try {

			HttpPost httpMethod = null;
			httpMethod = new HttpPost(url);

			String uidPwd = IconAPIContext.getApiUserID() + ":" + IconAPIContext.getApiUserPwd();
			String uidPwdEncoded = new String(Base64.encodeBase64(uidPwd.getBytes()));
			httpMethod.setHeader("Authorization", "Basic " + uidPwdEncoded);
			
			httpMethod.setHeader("user", IconAPIContext.getApiUserID() + ":" + IconAPIContext.getApiUserPwd());

			if (formValues != null && formValues.size() > 0) {
				HttpEntity e = new UrlEncodedFormEntity(formValues, "utf-8");
				httpMethod.setEntity(e);
			}

			if (IconAPIContext.debugMode) {
				System.out.println("ICON API POST Method: " + url);
				for (NameValuePair nvp : formValues) {
					System.out.print("\tNAME: " + nvp.getName() + " VALUE: " + nvp.getValue());
				}

				System.out.println();
			}

			HttpResponse response = httpClient.execute(httpMethod);

			HttpEntity entity = response.getEntity();
			if (entity != null) {
				InputStream is = entity.getContent();
				try {
					StringBuilder sb = new StringBuilder();
					BufferedReader rd = new BufferedReader(new InputStreamReader(is));
					String line;
					while ((line = rd.readLine()) != null) {
						sb.append(line);
					}
					rd.close();
					responseJSON = sb.toString();
					// do something useful with the response
				} catch (IOException ex) {
					// In case of an IOException the connection will be released
					// back to the connection manager automatically
					throw ex;
				} catch (RuntimeException ex) {
					// In case of an unexpected exception you may want to abort
					// the HTTP request in order to shut down the underlying
					// connection immediately.
					httpMethod.abort();
					throw ex;
				} finally {
					// Closing the input stream will trigger connection release
					try {
						is.close();
					} catch (Exception ignore) {
					}
				}
			}

			EntityUtils.consume(entity);

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			// When HttpClient instance is no longer needed,
			// shut down the connection manager to ensure
			// immediate deallocation of all system resources
			httpClient.getConnectionManager().shutdown();

		}
		if (IconAPIContext.debugMode) {
			System.out.println("ICON API Response: " + responseJSON);
		}

		return responseJSON;
	}

	/**
	 * 
	 * @return The base url ending in '/'
	 * 
	 */
	protected String getBaseAPIAddressURL() {
		StringBuilder sb = new StringBuilder(IconAPIContext.getEndPointAddressURL());

		// the endpoint should end with a '/' so add it if it doesn't have it
		if (sb.charAt(sb.length() - 1) != '/') {
			sb.append("/");
		}

		return sb.toString();
	}

	protected String getSecureBaseAPIAddressURL() {
		StringBuilder sb = new StringBuilder(IconAPIContext.getEndPointSecureAddressURL());

		// the endpoint should end with a '/' so add it if it doesn't have it
		if (sb.charAt(sb.length() - 1) != '/') {
			sb.append("/");
		}

		return sb.toString();
	}

	protected String makeAPIGetRequest(String url) throws Exception {
		return this.makeAPIGetRequest(url, true);
	}

	protected String getSecureSelfServeAPIURL() {
		StringBuilder sb = new StringBuilder(IconAPIContext.getEndPointSelfServeSecureURL());
	
		// the endpoint should end with a '/' so add it if it doesn't have it
		if (sb.charAt(sb.length() - 1) != '/') {
			sb.append("/");
		}
	
		return sb.toString().trim();
	}
}
