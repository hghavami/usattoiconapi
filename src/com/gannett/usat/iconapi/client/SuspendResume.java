package com.gannett.usat.iconapi.client;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsDeserializer;
import com.gannett.usat.iconapi.domainbeans.suspendResume.SuspendResumeResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * This class is used to communicate with the ICON API for vacation stops/starts
 * 
 * @author aeast
 * 
 */
public class SuspendResume extends IconAPIBase {

	/**
	 * Converts json returned from the ICON Subscription API to Objects
	 * 
	 * @param json
	 *            - A json string as returned from the API
	 * @return Collection of SubscriberAccount objects
	 */
	public static SuspendResumeResponse jsonToSuspendResumeResponse(String json) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Errors.class, new ErrorsDeserializer());
		Gson gson = gsonBuilder.create();

		SuspendResumeResponse response = null;

		Type suspendResumeResponseType = new TypeToken<SuspendResumeResponse>() {
		}.getType();

		try {
			response = gson.fromJson(json, suspendResumeResponseType);
		} catch (Exception e) {
			e.printStackTrace();
			if (response == null) {
				response = new SuspendResumeResponse();
			}
			response.setRawResponse(json);
		}

		return response;
	}

	/**
	 * 
	 * @param pubCode
	 * @param accountNumber
	 * @param effectiveDate
	 *            format should be in YYYYMMDD
	 * @param messageCode
	 *            The code from MSGTAB table
	 * @return
	 * @throws Exception
	 */
	public SuspendResumeResponse createSuspendResume(String pubCode, String accountNumber, String stopDate, String startDate,
			String vacationOption) throws Exception {

		String responseJSON = null;

		URL url = new URL(this.getBaseAPIURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(SUSPEND_RESUME_PATH);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		if (IconAPIContext.getApiGCIUnitNumber() != null) {
			nameValuePairs.add(new BasicNameValuePair("gci_unit", IconAPIContext.getApiGCIUnitNumber()));
		}

		if (pubCode != null) {
			nameValuePairs.add(new BasicNameValuePair("pub_code", pubCode));
		}

		if (accountNumber != null) {
			nameValuePairs.add(new BasicNameValuePair("account_number", accountNumber));
		}

		if (stopDate != null) {
			nameValuePairs.add(new BasicNameValuePair("stop_date", stopDate));
		}

		if (startDate != null) {
			nameValuePairs.add(new BasicNameValuePair("start_date", startDate));
		}
		if (vacationOption != null) {
			nameValuePairs.add(new BasicNameValuePair("vacation_option", vacationOption));
		}

		// build up URL
		java.net.URI uri = builder.build();

		String uriString = uri.toString();

		responseJSON = this.makeAPIPostRequest(uriString, nameValuePairs);

		SuspendResumeResponse response = SuspendResume.jsonToSuspendResumeResponse(responseJSON);

		return response;

	}
}
