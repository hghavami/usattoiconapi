package com.gannett.usat.iconapi.client;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsDeserializer;
import com.gannett.usat.iconapi.domainbeans.changeCard.ChangeCardEzPayResponse;
import com.gannett.usat.iconapi.domainbeans.changeCard.ChangeCardResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * This class is used to communicate with the ICON API for vacation stops/starts
 * 
 * @author aeast
 * 
 */
public class ChangeCard extends IconAPIBase {

	/**
	 * Converts json returned from the ICON Subscription API to Objects
	 * 
	 * @param json
	 *            - A json string as returned from the API
	 * @return Collection of SubscriberAccount objects
	 */
	public static ChangeCardResponse jsonToChangeCardResponse(String json) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Errors.class, new ErrorsDeserializer());
		Gson gson = gsonBuilder.create();

		ChangeCardResponse response = null;

		Type changeCardResponseType = new TypeToken<ChangeCardResponse>() {
		}.getType();

		try {
			response = gson.fromJson(json, changeCardResponseType);
		} catch (Exception e) {
			e.printStackTrace();
			if (response == null) {
				response = new ChangeCardResponse();
			}
			response.setRawResponse(json);
		}

		return response;
	}

	/**
	 * 
	 * @param pubCode
	 * @param accountNumber
	 * @param effectiveDate
	 *            format should be in YYYYMMDD
	 * @param messageCode
	 *            The code from MSGTAB table
	 * @return
	 * @throws Exception
	 */
	public ChangeCardResponse createChangeCard(String pubCode, String accountNumber, String cardNumber, String cardType,
			String cardExp, String ezPay) throws Exception {

		String responseJSON = null;

//		URL url = new URL(this.getBaseAPIURL());
		URL url = new URL(this.getSecureSelfServeAPIURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(CHANGE_CARD_PATH);
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		if (IconAPIContext.getApiGCIUnitNumber() != null) {
			nameValuePairs.add(new BasicNameValuePair("gci_unit", IconAPIContext.getApiGCIUnitNumber()));
		}

		if (pubCode != null) {
			nameValuePairs.add(new BasicNameValuePair("pub_code", pubCode));
		}

		if (accountNumber != null) {
			nameValuePairs.add(new BasicNameValuePair("account_number", accountNumber));
		}

		if (cardNumber != null) {
			nameValuePairs.add(new BasicNameValuePair("card_number", cardNumber));
		}

		if (cardType != null) {
			nameValuePairs.add(new BasicNameValuePair("card_type", cardType));
		}
		if (cardExp != null) {
			nameValuePairs.add(new BasicNameValuePair("card_exp", cardExp));
		}

		// build up URL
		java.net.URI uri = builder.build();

		String uriString = uri.toString();

		responseJSON = this.makeAPIPostRequest(uriString, nameValuePairs);

		ChangeCardResponse response = ChangeCard.jsonToChangeCardResponse(responseJSON);

		return response;

	}

	/**
	 * 
	 * @param pubCode
	 * @param accountNumber
	 * @param effectiveDate
	 *            format should be in YYYYMMDD
	 * @param messageCode
	 *            The code from MSGTAB table
	 * @return
	 * @throws Exception
	 */
	public ChangeCardEzPayResponse createChangeCardEzPay(String pubCode, String accountNumber, String cardNumber, String cardType,
			String cardExp, String ezPay) throws Exception {

		String responseJSON = null;

//		URL url = new URL(this.getBaseAPIURL());
		URL url = new URL(this.getSecureSelfServeAPIURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(CHANGE_CARD_EZPAY_PATH);
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		if (IconAPIContext.getApiGCIUnitNumber() != null) {
			nameValuePairs.add(new BasicNameValuePair("gci_unit", IconAPIContext.getApiGCIUnitNumber()));
		}

		if (pubCode != null) {
			nameValuePairs.add(new BasicNameValuePair("pub_code", pubCode));
		}

		if (accountNumber != null) {
			nameValuePairs.add(new BasicNameValuePair("account_number", accountNumber));
		}

		if (cardNumber != null) {
			nameValuePairs.add(new BasicNameValuePair("card_number", cardNumber));
		}

		if (cardType != null) {
			nameValuePairs.add(new BasicNameValuePair("card_type", cardType));
		}
		if (cardExp != null) {
			nameValuePairs.add(new BasicNameValuePair("card_exp", cardExp));
		}

		// build up URL
		java.net.URI uri = builder.build();

		String uriString = uri.toString();

		responseJSON = this.makeAPIPostRequest(uriString, nameValuePairs);

		ChangeCardEzPayResponse response = ChangeCard.jsonToChangeCardEzPayResponse(responseJSON);

		return response;

	}

	/**
	 * Converts json returned from the ICON Subscription API to Objects
	 * 
	 * @param json
	 *            - A json string as returned from the API
	 * @return Collection of SubscriberAccount objects
	 */
	public static ChangeCardEzPayResponse jsonToChangeCardEzPayResponse(String json) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Errors.class, new ErrorsDeserializer());
		Gson gson = gsonBuilder.create();

		ChangeCardEzPayResponse response = null;

		Type changeCardEzPayResponseType = new TypeToken<ChangeCardEzPayResponse>() {
		}.getType();

		try {
			response = gson.fromJson(json, changeCardEzPayResponseType);
		} catch (Exception e) {
			e.printStackTrace();
			if (response == null) {
				response = new ChangeCardEzPayResponse();
			}
			response.setRawResponse(json);
		}

		return response;
	}
}
