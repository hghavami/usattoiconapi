/**
 * 
 */
package com.gannett.usat.iconapi.client;

import java.net.URL;
import java.net.URLEncoder;

import org.apache.http.client.utils.URIBuilder;

import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsDeserializer;
import com.gannett.usat.iconapi.domainbeans.disclaimers.DisclaimersTextsWrapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author hghavami
 * 
 */
public class Disclaimers extends IconAPIBase {

	/**
	 * 
	 */
	public Disclaimers() {
		super();
	}

	/**
	 * Converts json returned from the ICON Subscription API to Objects
	 * 
	 * @param json
	 *            - A json string as returned from the API
	 * @return Collection of SubscriberAccount objects
	 */
	public static DisclaimersTextsWrapper jsonToDisclaimerTexts(String json) {
		try {
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.registerTypeAdapter(Errors.class, new ErrorsDeserializer());
			Gson gson = gsonBuilder.create();

//			Type collectionType = new TypeToken<Collection<DisclaimerTexts>>() {}.getType();
//			Collection<DisclaimerTexts> texts = gson.fromJson(json, collectionType);
			DisclaimersTextsWrapper texts = gson.fromJson(json, DisclaimersTextsWrapper.class);
			return texts;
		} catch (Exception e) {
			System.out.println("API response was not JSON and could not be understood because: " + e);
			return null;
		}
	}

	/**
	 * 
	 * @return Raw JSON response as a String object
	 * @throws Exception
	 */
	public String getDisclaimers(String pubCode, String fodCode, String rateCode, String deliveryMethod, String ezPayFlag)
			throws Exception {
		String responseJSON = null;

		URL url = new URL(this.getBaseAPIURL());

		URIBuilder builder = new URIBuilder();

		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(DISCLAIMERS);
		if (fodCode == null) {
			fodCode = "";
		}
		// remove hardcode hdcons-153 hghavami
		builder.addParameter("gci_unit", IconAPIContext.getApiGCIUnitNumber());
		// builder.addParameter("gci_unit",
		// IconAPIContext.getApiGCIUnitNumber());
		builder.addParameter("pub_code", pubCode);
		builder.addParameter("fod_code", fodCode);
		builder.addParameter("start_date", "");
		builder.addParameter("promotion_code", "");

		// Check and build up URL
		java.net.URI uri = builder.build();
		if (rateCode == null) {
			rateCode = "";
		}
		if (deliveryMethod == null) {
			deliveryMethod = "";
		}
		if (ezPayFlag == null) {
			ezPayFlag = "";
		}

		StringBuilder uriString = new StringBuilder(uri.toString());
		uriString.append("&rate_code=").append(URLEncoder.encode(rateCode, "UTF-8"));
		uriString.append("&delivery_method=").append(URLEncoder.encode(deliveryMethod, "UTF-8"));
		uriString.append("&ez_pay_flag=").append(URLEncoder.encode(ezPayFlag, "UTF-8"));

		responseJSON = this.makeAPIGetRequest(uriString.toString());

		return responseJSON;

	}

	/**
	 * This method returns objects created from the api response
	 * 
	 * @param emailAddress
	 *            - Used to query Genesys
	 * @return A collection of com SubscriberAccount beans that can be used for
	 *         setting up business objects or displayed on a screen If you
	 *         already have a json string from calling getAccountDataForEmail,
	 *         you should use the helper method to convert it to objects
	 * @throws Exception
	 */

	// Genesys hdcons-153 hghavami used for testing
	public DisclaimersTextsWrapper getDisclaimersObjects(String pubCode, String fodCode, String rateCode,
			String deliveryMethod, String ezPayFlag) throws Exception {

		String json = this.getDisclaimers(pubCode, fodCode, rateCode, deliveryMethod, ezPayFlag);

		return Disclaimers.jsonToDisclaimerTexts(json);
	}

}
