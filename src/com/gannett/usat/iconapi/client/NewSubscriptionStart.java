package com.gannett.usat.iconapi.client;

import java.lang.reflect.Type;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.joda.time.DateTime;

import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsDeserializer;
import com.gannett.usat.iconapi.domainbeans.newSubscriptionStart.StartSubscriptionResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class NewSubscriptionStart extends IconAPIBase {

	/**
	 * 
	 * @param json
	 * @return
	 */
	public static StartSubscriptionResponse jsonToStartSubscriptionResponse(String json) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Errors.class, new ErrorsDeserializer());
		Gson gson = gsonBuilder.create();

		StartSubscriptionResponse response = null;

		Type responseType = new TypeToken<StartSubscriptionResponse>() {
		}.getType();

		try {
			response = gson.fromJson(json, responseType);
		} catch (Exception e) {
			e.printStackTrace();
			if (response == null) {
				response = new StartSubscriptionResponse();
			}
			response.setRawResponse(json);
		}

		return response;
	}

	/**
	 * 
	 * @param pubCode
	 * @param cardNumber
	 * @param cardType
	 * @param cardExpiration
	 * @param startDate
	 * @param orderID
	 * @param sourceCode
	 * @param promotionCode
	 * @param contestCode
	 * @param rateCode
	 * @param phone
	 * @param secondaryPhone
	 * @param deliveryFirstName
	 * @param deliveryLastName
	 * @param email
	 * @param secondaryEmail
	 * @param deliveryFirmName
	 * @param deliveryAddress1
	 * @param deliveryAdditionalAddress
	 * @param deliveryCity
	 * @param deliveryState
	 * @param deliveryZip
	 * @param billingFirstName
	 * @param billingLastName
	 * @param billingFirmName
	 * @param billingAddress1
	 * @param billingAdditionalAddress
	 * @param billingCity
	 * @param billingState
	 * @param billingZip
	 * @param isGift
	 * @param isEZPAY
	 * @param isOneTimeBill
	 * @param carrierTipAmount
	 * @param nieDonationAmount
	 * @return StartSubscriptionResponse
	 * @throws Exception
	 */
	public StartSubscriptionResponse createNewStart(String pubCode, String cardNumber, String cardType, String cardExpiration,
			DateTime startDate, String orderID, String sourceCode, String promotionCode, String contestCode, String rateCode,
			String phone, String secondaryPhone, String deliveryFirstName, String deliveryLastName, String email,
			String secondaryEmail, String deliveryFirmName, String deliveryAddress1, String deliveryAdditionalAddress,
			String deliveryCity, String deliveryState, String deliveryZip, String billingFirstName, String billingLastName,
			String billingFirmName, String billingAddress1, String billingAdditionalAddress, String billingCity,
			String billingState, String billingZip, String billingPhone, boolean isGift, boolean isEZPAY, boolean isOneTimeBill,
			String carrierTipAmount, String nieDonationAmount, String fodCode, String deliveryMethod, String termLength,
			String ratePeriod) throws Exception {

		String responseJSON = null;

//		URL url = new URL(this.getSecureBaseAPIURL());
		URL url = new URL(this.getSecureSelfServeAPIURL());
		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort())
				.setPath(IconAPIBase.NEW_SUBSCRIPTION_START_PATH);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		if (IconAPIContext.getApiGCIUnitNumber() != null) {
			nameValuePairs.add(new BasicNameValuePair("gci_unit", IconAPIContext.getApiGCIUnitNumber()));
		}

		if (pubCode != null) {
			nameValuePairs.add(new BasicNameValuePair("pub_code", pubCode));
		}

		if (cardNumber != null) {
			nameValuePairs.add(new BasicNameValuePair("card_number", cardNumber));
		}

		if (cardType != null) {
			nameValuePairs.add(new BasicNameValuePair("card_type", cardType));
		}

		if (cardExpiration != null) {
			nameValuePairs.add(new BasicNameValuePair("card_exp", cardExpiration));
		}

		if (carrierTipAmount != null) {
			nameValuePairs.add(new BasicNameValuePair("carrier_tip", carrierTipAmount));
		} else {
			nameValuePairs.add(new BasicNameValuePair("carrier_tip", "0.00"));
		}
		if (nieDonationAmount != null) {
			nameValuePairs.add(new BasicNameValuePair("nie_donation", nieDonationAmount));
		} else {
			nameValuePairs.add(new BasicNameValuePair("nie_donation", "0.00"));
		}

		if (startDate != null) {
			nameValuePairs.add(new BasicNameValuePair("start_date", startDate.toString("yyyyMMdd")));
		}

		if (orderID != null) {
			nameValuePairs.add(new BasicNameValuePair("order_id", orderID));
		}

		if (sourceCode != null) {
			nameValuePairs.add(new BasicNameValuePair("source_code", sourceCode));
		}

		if (promotionCode != null) {
			nameValuePairs.add(new BasicNameValuePair("promotion_code", promotionCode));
		}

		if (contestCode != null) {
			nameValuePairs.add(new BasicNameValuePair("contest_code", contestCode));
		}

		if (rateCode != null) {
			nameValuePairs.add(new BasicNameValuePair("rate_code", rateCode));
		}

		if (phone != null) {
			nameValuePairs.add(new BasicNameValuePair("phone", phone));
		}
		if (secondaryPhone != null) {
			nameValuePairs.add(new BasicNameValuePair("secondary_phone", secondaryPhone));
		}
		if (deliveryFirstName != null) {
			nameValuePairs.add(new BasicNameValuePair("first_name", deliveryFirstName));
		}
		if (deliveryLastName != null) {
			nameValuePairs.add(new BasicNameValuePair("last_name", deliveryLastName));
		}
		if (email != null) {
			nameValuePairs.add(new BasicNameValuePair("email", email));
		}
		if (secondaryEmail != null) {
			nameValuePairs.add(new BasicNameValuePair("secondary_email", secondaryEmail));
		}

		if (deliveryFirmName != null) {
//			nameValuePairs.add(new BasicNameValuePair("firm_name", deliveryFirmName));
			nameValuePairs.add(new BasicNameValuePair("address_line_2", deliveryFirmName));			
		}
		if (deliveryAddress1 != null) {
			nameValuePairs.add(new BasicNameValuePair("address_line_1", deliveryAddress1));
		}
		if (deliveryAdditionalAddress != null) {
//			nameValuePairs.add(new BasicNameValuePair("address_line_2", deliveryAdditionalAddress));
			nameValuePairs.add(new BasicNameValuePair("additional_delivery_info", deliveryAdditionalAddress));			
		}
		if (deliveryCity != null) {
			nameValuePairs.add(new BasicNameValuePair("city_name", deliveryCity));
		}
		if (deliveryState != null) {
			nameValuePairs.add(new BasicNameValuePair("state", deliveryState));
		}
		if (deliveryZip != null) {
			nameValuePairs.add(new BasicNameValuePair("zip_code", deliveryZip));
		}

		if (billingFirstName != null) {
			nameValuePairs.add(new BasicNameValuePair("billing_first_name", billingFirstName));
		}
		if (billingLastName != null) {
			nameValuePairs.add(new BasicNameValuePair("billing_last_name", billingLastName));
		}
		if (billingFirmName != null) {
			nameValuePairs.add(new BasicNameValuePair("billing_firm_name", billingFirmName));
		}
		if (billingAddress1 != null) {
			nameValuePairs.add(new BasicNameValuePair("billing_address_line_1", billingAddress1));
		}
		if (billingAdditionalAddress != null) {
			nameValuePairs.add(new BasicNameValuePair("billing_address_line_2", billingAdditionalAddress));
		}
		if (billingCity != null) {
			nameValuePairs.add(new BasicNameValuePair("billing_city_name", billingCity));
		}
		if (billingState != null) {
			nameValuePairs.add(new BasicNameValuePair("billing_state", billingState));
		}
		if (billingZip != null) {
			nameValuePairs.add(new BasicNameValuePair("billing_zip_code", billingZip));
		}
		if (billingPhone != null) {
			nameValuePairs.add(new BasicNameValuePair("billing_phone", billingPhone));
		}

		if (isEZPAY) {
			nameValuePairs.add(new BasicNameValuePair("ez_pay_flag", "Y"));
		} else {
			nameValuePairs.add(new BasicNameValuePair("ez_pay_flag", "N"));
		}
		if (isGift) {
			nameValuePairs.add(new BasicNameValuePair("gift_payer_flag", "Y"));
		} else {
			nameValuePairs.add(new BasicNameValuePair("gift_payer_flag", "N"));
		}
		if (isOneTimeBill) {
			nameValuePairs.add(new BasicNameValuePair("one_time_bill_flag", "Y"));
		} else {
			nameValuePairs.add(new BasicNameValuePair("one_time_bill_flag", "N"));
		}

		if (fodCode != null) {
			nameValuePairs.add(new BasicNameValuePair("fod_code", fodCode));
		}
		if (deliveryMethod != null) {
			nameValuePairs.add(new BasicNameValuePair("delivery_method", deliveryMethod));
		}
		if (termLength != null) {
			nameValuePairs.add(new BasicNameValuePair("subscription_length", termLength));
		}
		if (ratePeriod != null) {
			nameValuePairs.add(new BasicNameValuePair("rate_period", ratePeriod));
		}

		nameValuePairs.add(new BasicNameValuePair("pia_rate_code_flag", "Y"));

		// build up URL
		URI uri = builder.build();

		String uriString = uri.toString();

		responseJSON = this.makeAPIPostRequest(uriString, nameValuePairs);

		StartSubscriptionResponse response = NewSubscriptionStart.jsonToStartSubscriptionResponse(responseJSON);

		return response;

	}

}
