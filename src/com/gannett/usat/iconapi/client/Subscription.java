package com.gannett.usat.iconapi.client;

import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Collection;

import org.apache.http.client.utils.URIBuilder;

import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsDeserializer;
import com.gannett.usat.iconapi.domainbeans.subscriberAccount.SubscriberAccount;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class Subscription extends IconAPIBase {

	public Subscription() {
		super();

	}

	/**
	 * Converts json returned from the ICON Subscription API to Objects
	 * 
	 * @param json
	 *            - A json string as returned from the API
	 * @return Collection of SubscriberAccount objects
	 */
	public static Collection<SubscriberAccount> jsonToSubscriberAccount(String json) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Errors.class, new ErrorsDeserializer());
		Gson gson = gsonBuilder.create();

		Type collectionType = new TypeToken<Collection<SubscriberAccount>>() {
		}.getType();
		Collection<SubscriberAccount> accounts = gson.fromJson(json, collectionType);

		return accounts;
	}

	/**
	 * 
	 * @param emailAddress
	 *            - The email address for whom we want accounts.
	 * @return Raw JSON response as a String object
	 * @throws Exception
	 */
	public String getAccountDataForEmail(String emailAddress) throws Exception {
		String responseJSON = null;

		URL url = new URL(this.getBaseAPIURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(SUBSCRIPTIONS_PATH);
		builder.addParameter("gci_unit", IconAPIContext.getApiGCIUnitNumber());
		builder.addParameter("email", "");

		// build up URL
		java.net.URI uri = builder.build();

		// for some reason when we use setParameter above for email it double encodes the '@' in the email address.
		String uriString = uri.toString();
		uriString = uriString + URLEncoder.encode(emailAddress, "UTF-8");

		responseJSON = this.makeAPIGetRequest(uriString);

		return responseJSON;

	}

	/**
	 * This method returns objects created from the api response
	 * 
	 * @param emailAddress
	 *            - Used to query Genesys
	 * @return A collection of com SubscriberAccount beans that can be used for setting up business objects or displayed on a screen
	 *         If you already have a json string from calling getAccountDataForEmail, you should use the helper method to convert it
	 *         to objects
	 * @throws Exception
	 */
	public Collection<SubscriberAccount> getAccountObjectsForEmail(String emailAddress) throws Exception {

		String json = this.getAccountDataForEmail(emailAddress);

		return Subscription.jsonToSubscriberAccount(json);
	}

	public String getAccountDataForAccountNumber(String accountNumber, String pubCode) throws Exception {
		String responseJSON = null;

		URL url = new URL(this.getBaseAPIURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(SUBSCRIPTIONS_PATH);
		builder.addParameter("gci_unit", IconAPIContext.getApiGCIUnitNumber());
		if (pubCode != null && !pubCode.trim().equals("")) {
			builder.addParameter("pub_code", pubCode);
		}
		builder.addParameter("account_number", "");
		// build up URL
		java.net.URI uri = builder.build();

		if (accountNumber == null || accountNumber.length() > 10) {
			throw new Exception("Subscription::getAccountDataForAccountNumber() Invalid Account Number:" + accountNumber);
		}
		// for some reason when we use setParameter above for email it double encodes the '@' in the email address.
		String uriString = uri.toString();
		uriString = uriString + URLEncoder.encode(accountNumber, "UTF-8");

		responseJSON = this.makeAPIGetRequest(uriString);

		return responseJSON;

	}

	/**
	 * 
	 * @param accountNumber
	 *            - The account number interested in
	 * @return A collection of com SubscriberAccount beans that can be used for setting up business objects or displayed on a screen
	 *         If you already have a json string from calling getAccountDataForEmail, you should use the helper method to convert it
	 *         to objects
	 * @throws Exception
	 */
	public Collection<SubscriberAccount> getAccountObjectsForAccountNumber(String accountNumber) throws Exception {

		String json = this.getAccountDataForEmail(accountNumber);

		return Subscription.jsonToSubscriberAccount(json);
	}

	public String getAccountDataForHomePhoneZip(String phoneNumber, String zipCode) throws Exception {
		String responseJSON = null;

		URL url = new URL(this.getBaseAPIURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(SUBSCRIPTIONS_PATH);
		builder.addParameter("gci_unit", IconAPIContext.getApiGCIUnitNumber());
		builder.addParameter("home_phone", phoneNumber);
		builder.addParameter("delivery_zip", "");
		// build up URL
		java.net.URI uri = builder.build();

		if (phoneNumber == null || phoneNumber.length() > 10 || zipCode == null || zipCode.length() > 5) {
			throw new Exception("Subscription::getAccountDataForHomePhoneZip() Invalid Home Phone/Zip Code:" + phoneNumber
					+ zipCode);
		}
		// for some reason when we use setParameter above for email it double encodes the '@' in the email address.
		String uriString = uri.toString();
		uriString = uriString + URLEncoder.encode(zipCode, "UTF-8");

		responseJSON = this.makeAPIGetRequest(uriString);

		return responseJSON;

	}

	public String getAccountDataForBillingPhoneZip(String phoneNumber, String zipCode) throws Exception {
		String responseJSON = null;

		URL url = new URL(this.getBaseAPIURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(SUBSCRIPTIONS_PATH);
		builder.addParameter("gci_unit", IconAPIContext.getApiGCIUnitNumber());
		builder.addParameter("billing_phone", phoneNumber);
		builder.addParameter("delivery_zip", "");
		// build up URL
		java.net.URI uri = builder.build();

		if (phoneNumber == null || phoneNumber.length() > 10 || zipCode == null || zipCode.length() > 5) {
			throw new Exception("Subscription::getAccountDataForBillingPhoneZip() Invalid Billing Phone/Zip Code:" + phoneNumber
					+ zipCode);
		}
		// for some reason when we use setParameter above for email it double encodes the '@' in the email address.
		String uriString = uri.toString();
		uriString = uriString + URLEncoder.encode(zipCode, "UTF-8");

		responseJSON = this.makeAPIGetRequest(uriString);

		return responseJSON;

	}

}
