package com.gannett.usat.iconapi.domainbeans.addressChange;

import java.io.Serializable;

import com.gannett.usat.iconapi.domainbeans.Errors;

public class AddressChange implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6573165173327487242L;

	private String account_number = null;
	private String delivery_method = null;
	private String gci_unit = null;
	private String pub_code = null;
	private int trnnbr = 0;
	private Errors errors = null;

	public String getAccount_number() {
		return account_number;
	}

	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}

	public String getDelivery_method() {
		return delivery_method;
	}

	public void setDelivery_method(String delivery_method) {
		this.delivery_method = delivery_method;
	}

	public String getGci_unit() {
		return gci_unit;
	}

	public void setGci_unit(String gci_unit) {
		this.gci_unit = gci_unit;
	}

	public String getPub_code() {
		return pub_code;
	}

	public void setPub_code(String pub_code) {
		this.pub_code = pub_code;
	}

	public int getTrnnbr() {
		return trnnbr;
	}

	public void setTrnnbr(int trnnbr) {
		this.trnnbr = trnnbr;
	}

	public Errors getErrors() {
		return errors;
	}

	public void setErrors(Errors errors) {
		this.errors = errors;
	}

}
