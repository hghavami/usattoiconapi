package com.gannett.usat.iconapi.domainbeans.addressChange;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.iconapi.domainbeans.Error;
import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.GenesysBaseAPIResponse;

public class AddressChangeResponse extends GenesysBaseAPIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1962459116261191378L;

	private AddressChange change_of_subscription = null;

	@Override
	public boolean containsErrors() {
		boolean containsErrors = false;
		if (this.change_of_subscription != null) {
			Errors errors = this.change_of_subscription.getErrors();
			if (errors != null) {
				if (errors.getErrors() != null) {
					if (errors.getErrors().size() > 0) {
						containsErrors = true;
					}
				}
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				containsErrors = true;
			}
		}
		return containsErrors;
	}

	@Override
	public Collection<String> getErrorMessages() {
		Collection<String> messages = new ArrayList<String>();
		if (this.change_of_subscription != null) {
			Errors errors = this.change_of_subscription.getErrors();
			if (errors != null) {
				if (errors.getErrors() != null) {
					for (Error e : errors.getErrors()) {
						String aMessage = e.getValue();
						messages.add(aMessage);
					}
				}
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				messages.add(this.getRawResponse());
			}
		}
		return messages;
	}

	public AddressChange getChange_of_subscription() {
		return change_of_subscription;
	}

	public void setChange_of_subscription(AddressChange change_of_subscription) {
		this.change_of_subscription = change_of_subscription;
	}

}
