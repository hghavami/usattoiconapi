package com.gannett.usat.iconapi.domainbeans.Code1Validation;

import java.io.Serializable;

import com.gannett.usat.iconapi.domainbeans.GenesysAddressAPIResponse;

public class Code1Validation extends GenesysAddressAPIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5088138208668592631L;

	private String usps_record = null;
	private String vintage_date = null;
	private String house_unit = null;
	private String pre_direction = null;
	private String street = null;
	private String box_number = null;
	private String street_type = null;
	private String post_direction = null;
	private String sub_type = null;
	private String sub_number = null;
	private String city = null;
	private String short_city = null;
	private String city_code = null;
	private String firm_name = null;
	private String state = null;
	private String zip = null;
	private String zip_4 = null;
	private String delivery_point = null;
	private String cris_code = null;
	private String delivery_pnt = null;
	private String congress = null;
	private String lot_code = null;
	private String resultant = null;
	private String usps_delivery = null;
	private String error_text = null;
	private String grade = null;
	private String code1d = null;
	private String dropped = null;
	private String fips_state = null;
	private String fips_county = null;
	private String error_code = null;

	public Code1Validation() {
		super();
	}

	public String getUsps_record() {
		return usps_record;
	}

	public void setUsps_record(String gci_unit) {
		this.usps_record = gci_unit;
	}

	public String getVintage_date() {
		return vintage_date;
	}

	public void setVintage_date(String account_number) {
		this.vintage_date = account_number;
	}

	public String getHouse_unit() {
		return house_unit;
	}

	public void setHouse_unit(String billing_address_city) {
		this.house_unit = billing_address_city;
	}

	public String getPre_direction() {
		return pre_direction;
	}

	public void setPre_direction(String billing_address_line_1) {
		this.pre_direction = billing_address_line_1;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String billing_address_line_2) {
		this.street = billing_address_line_2;
	}

	public String getBox_number() {
		return box_number;
	}

	public void setBox_number(String billing_address_state) {
		this.box_number = billing_address_state;
	}

	public String getPost_direction() {
		return post_direction;
	}

	public void setPost_direction(String billing_address_zip_code) {
		this.post_direction = billing_address_zip_code;
	}

	public String getSub_type() {
		return sub_type;
	}

	public void setSub_type(String billing_firm_name) {
		this.sub_type = billing_firm_name;
	}

	public String getSub_number() {
		return sub_number;
	}

	public void setSub_number(String billing_first_name) {
		this.sub_number = billing_first_name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String billing_last_name) {
		this.city = billing_last_name;
	}

	public String getShort_city() {
		return short_city;
	}

	public void setShort_city(String billing_phone) {
		this.short_city = billing_phone;
	}

	public String getCity_code() {
		return city_code;
	}

	public void setCity_code(String contest_code) {
		this.city_code = contest_code;
	}

	public String getState() {
		return state;
	}

	public void setState(String credit_card_expiration_date) {
		this.state = credit_card_expiration_date;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String credit_card_last_four) {
		this.zip = credit_card_last_four;
	}

	public String getZip_4() {
		return zip_4;
	}

	public void setZip_4(String credit_card_type) {
		this.zip_4 = credit_card_type;
	}

	public String getDelivery_point() {
		return delivery_point;
	}

	public void setDelivery_point(String current_pia_type) {
		this.delivery_point = current_pia_type;
	}

	public String getCris_code() {
		return cris_code;
	}

	public void setCris_code(String current_rate_code) {
		this.cris_code = current_rate_code;
	}

	public String getDelivery_pnt() {
		return delivery_pnt;
	}

	public void setDelivery_pnt(String delivery_address_city) {
		this.delivery_pnt = delivery_address_city;
	}

	public String getCongress() {
		return congress;
	}

	public void setCongress(String delivery_address_line_1) {
		this.congress = delivery_address_line_1;
	}

	public String getLot_code() {
		return lot_code;
	}

	public void setLot_code(String delivery_address_line_2) {
		this.lot_code = delivery_address_line_2;
	}

	public String getResultant() {
		return resultant;
	}

	public void setResultant(String delivery_address_state) {
		this.resultant = delivery_address_state;
	}

	public String getUsps_delivery() {
		return usps_delivery;
	}

	public void setUsps_delivery(String delivery_address_subunit) {
		this.usps_delivery = delivery_address_subunit;
	}

	public String getError_text() {
		return error_text;
	}

	public void setError_text(String delivery_address_zip_code) {
		this.error_text = delivery_address_zip_code;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String delivery_first_name) {
		this.grade = delivery_first_name;
	}

	public String getCode1d() {
		return code1d;
	}

	public void setCode1d(String delivery_last_name) {
		this.code1d = delivery_last_name;
	}

	public String getDropped() {
		return dropped;
	}

	public void setDropped(String delivery_method) {
		this.dropped = delivery_method;
	}

	public String getFips_county() {
		return fips_county;
	}

	public void setFips_county(String ez_pay) {
		this.fips_county = ez_pay;
	}

	public String getFirm_name() {
		return firm_name;
	}

	public void setFirm_name(String firm_name) {
		this.firm_name = firm_name;
	}

	public String getFips_state() {
		return fips_state;
	}

	public void setFips_state(String fod_code) {
		this.fips_state = fod_code;
	}

	public String getStreet_type() {
		return street_type;
	}

	public void setStreet_type(String street_type) {
		this.street_type = street_type;
	}

	public String getError_code() {
		return error_code;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}

}
