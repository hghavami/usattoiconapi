package com.gannett.usat.iconapi.domainbeans.Code1Validation;

import java.io.Serializable;

import com.gannett.usat.iconapi.domainbeans.GenesysAddressAPIResponse;

public class Code1ValidationWrapper extends GenesysAddressAPIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5088138208668592631L;

	public Code1ValidationWrapper() {
		super();
		this.address = new Code1Validation();
	}

	private Code1Validation address = null;

	public Code1Validation getAddress() {
		return address;
	}

	public void setAddress(Code1Validation code1Validation) {
		this.address = code1Validation;
	}

}
