package com.gannett.usat.iconapi.domainbeans.nameChange;

import java.io.Serializable;

import com.gannett.usat.iconapi.domainbeans.Errors;

public class NameChangeRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8332352321801491385L;
	private String gci_unit = null;
	private String account_number = null;
	private String pub_code = null;
	private Errors errors = null;
	private String address_type = null;
	private String first_name = null;
	private String last_name = null;

	private String trnnbr = null;

	public String getLastname() {
		return last_name;
	}

	public void setLastname(String last_name) {
		this.last_name = last_name;
	}

	public String getFirstname() {
		return first_name;
	}

	public void setFirstname(String first_name) {
		this.first_name = first_name;
	}

	public String getAddresstype() {
		return address_type;
	}

	public void setAddresstype(String address_type) {
		this.address_type = address_type;
	}

	public String getAcctnbr() {
		return account_number;
	}

	public void setAcctnbr(String account_number) {
		this.account_number = account_number;
	}

	public String getTrnnbr() {
		return trnnbr;
	}

	public void setTrnnbr(String trnnbr) {
		this.trnnbr = trnnbr;
	}

	public String getGci_unit() {
		return gci_unit;
	}

	public void setGci_unit(String gci_unit) {
		this.gci_unit = gci_unit;
	}

	public String getPub_code() {
		return pub_code;
	}

	public void setPub_code(String pub_code) {
		this.pub_code = pub_code;
	}

	public Errors getErrors() {
		return errors;
	}

	public void setErrors(Errors errors) {
		this.errors = errors;
	}
}
