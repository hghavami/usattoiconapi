package com.gannett.usat.iconapi.domainbeans.subscriberAccount;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.iconapi.domainbeans.Error;
import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.GenesysBaseAPIResponse;

public class SubscriberAccount extends GenesysBaseAPIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5088138208668592631L;

	private String gci_unit = null;
	private String account_number = null;
	private String billing_address_city = null;
	private String billing_address_line_1 = null;
	private String billing_address_line_2 = null;
	private String billing_address_state = null;
	private String billing_address_subunit = null;
	private String billing_address_zip_code = null;
	private String billing_firm_name = null;
	private String billing_first_name = null;
	private String billing_last_name = null;
	private String billing_phone = null;
	private String contest_code = null;
	private String credit_card_expiration_date = null;
	private String credit_card_last_four = null;
	private String credit_card_type = null;
	private String current_pia_type = null;
	private String current_rate_code = null;
	private String delivery_address_city = null;
	private String delivery_address_line_1 = null;
	private String delivery_address_line_2 = null;
	private String delivery_address_state = null;
	private String delivery_address_subunit = null;
	private String delivery_address_zip_code = null;
	private String delivery_first_name = null;
	private String delivery_last_name = null;
	private String delivery_method = null;
	private String fod_code = null;
	private String ez_pay = null;
	private String firm_name = null;
	private String home_phone = null;
	private String last_payment_date = null;
	private String last_stop_code = null;
	private String number_of_copies = null;
	private String pending_expiration_date = null;
	private String promo_code = null;
	private String pub_code = null;
	private String source_code = null;
	private String start_date = null;
	private String subscriber_status = null;
	private String vacation_flag = null;
	private String work_phone = null;
	private Errors errors = null;

	public SubscriberAccount() {
		super();
	}

	public String getGci_unit() {
		return gci_unit;
	}

	public void setGci_unit(String gci_unit) {
		this.gci_unit = gci_unit;
	}

	public String getAccount_number() {
		return account_number;
	}

	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}

	public String getBilling_address_city() {
		return billing_address_city;
	}

	public void setBilling_address_city(String billing_address_city) {
		this.billing_address_city = billing_address_city;
	}

	public String getBilling_address_line_1() {
		return billing_address_line_1;
	}

	public void setBilling_address_line_1(String billing_address_line_1) {
		this.billing_address_line_1 = billing_address_line_1;
	}

	public String getBilling_address_line_2() {
		return billing_address_line_2;
	}

	public void setBilling_address_line_2(String billing_address_line_2) {
		this.billing_address_line_2 = billing_address_line_2;
	}

	public String getBilling_address_state() {
		return billing_address_state;
	}

	public void setBilling_address_state(String billing_address_state) {
		this.billing_address_state = billing_address_state;
	}

	public String getBilling_address_subunit() {
		return billing_address_subunit;
	}

	public void setBilling_address_subunit(String billing_address_subunit) {
		this.billing_address_subunit = billing_address_subunit;
	}

	public String getBilling_address_zip_code() {
		return billing_address_zip_code;
	}

	public void setBilling_address_zip_code(String billing_address_zip_code) {
		this.billing_address_zip_code = billing_address_zip_code;
	}

	public String getBilling_firm_name() {
		return billing_firm_name;
	}

	public void setBilling_firm_name(String billing_firm_name) {
		this.billing_firm_name = billing_firm_name;
	}

	public String getBilling_first_name() {
		return billing_first_name;
	}

	public void setBilling_first_name(String billing_first_name) {
		this.billing_first_name = billing_first_name;
	}

	public String getBilling_last_name() {
		return billing_last_name;
	}

	public void setBilling_last_name(String billing_last_name) {
		this.billing_last_name = billing_last_name;
	}

	public String getBilling_phone() {
		return billing_phone;
	}

	public void setBilling_phone(String billing_phone) {
		this.billing_phone = billing_phone;
	}

	public String getContest_code() {
		return contest_code;
	}

	public void setContest_code(String contest_code) {
		this.contest_code = contest_code;
	}

	public String getCredit_card_expiration_date() {
		return credit_card_expiration_date;
	}

	public void setCredit_card_expiration_date(String credit_card_expiration_date) {
		this.credit_card_expiration_date = credit_card_expiration_date;
	}

	public String getCredit_card_last_four() {
		return credit_card_last_four;
	}

	public void setCredit_card_last_four(String credit_card_last_four) {
		this.credit_card_last_four = credit_card_last_four;
	}

	public String getCredit_card_type() {
		return credit_card_type;
	}

	public void setCredit_card_type(String credit_card_type) {
		this.credit_card_type = credit_card_type;
	}

	public String getCurrent_pia_type() {
		return current_pia_type;
	}

	public void setCurrent_pia_type(String current_pia_type) {
		this.current_pia_type = current_pia_type;
	}

	public String getCurrent_rate_code() {
		return current_rate_code;
	}

	public void setCurrent_rate_code(String current_rate_code) {
		this.current_rate_code = current_rate_code;
	}

	public String getDelivery_address_city() {
		return delivery_address_city;
	}

	public void setDelivery_address_city(String delivery_address_city) {
		this.delivery_address_city = delivery_address_city;
	}

	public String getDelivery_address_line_1() {
		return delivery_address_line_1;
	}

	public void setDelivery_address_line_1(String delivery_address_line_1) {
		this.delivery_address_line_1 = delivery_address_line_1;
	}

	public String getDelivery_address_line_2() {
		return delivery_address_line_2;
	}

	public void setDelivery_address_line_2(String delivery_address_line_2) {
		this.delivery_address_line_2 = delivery_address_line_2;
	}

	public String getDelivery_address_state() {
		return delivery_address_state;
	}

	public void setDelivery_address_state(String delivery_address_state) {
		this.delivery_address_state = delivery_address_state;
	}

	public String getDelivery_address_subunit() {
		return delivery_address_subunit;
	}

	public void setDelivery_address_subunit(String delivery_address_subunit) {
		this.delivery_address_subunit = delivery_address_subunit;
	}

	public String getDelivery_address_zip_code() {
		return delivery_address_zip_code;
	}

	public void setDelivery_address_zip_code(String delivery_address_zip_code) {
		this.delivery_address_zip_code = delivery_address_zip_code;
	}

	public String getDelivery_first_name() {
		return delivery_first_name;
	}

	public void setDelivery_first_name(String delivery_first_name) {
		this.delivery_first_name = delivery_first_name;
	}

	public String getDelivery_last_name() {
		return delivery_last_name;
	}

	public void setDelivery_last_name(String delivery_last_name) {
		this.delivery_last_name = delivery_last_name;
	}

	public String getDelivery_method() {
		return delivery_method;
	}

	public void setDelivery_method(String delivery_method) {
		this.delivery_method = delivery_method;
	}

	public String getEz_pay() {
		return ez_pay;
	}

	public void setEz_pay(String ez_pay) {
		this.ez_pay = ez_pay;
	}

	public String getFirm_name() {
		return firm_name;
	}

	public void setFirm_name(String firm_name) {
		this.firm_name = firm_name;
	}

	public String getHome_phone() {
		return home_phone;
	}

	public void setHome_phone(String home_phone) {
		this.home_phone = home_phone;
	}

	public String getLast_payment_date() {
		return last_payment_date;
	}

	public void setLast_payment_date(String last_payment_date) {
		this.last_payment_date = last_payment_date;
	}

	public String getLast_stop_code() {
		return last_stop_code;
	}

	public void setLast_stop_code(String last_stop_code) {
		this.last_stop_code = last_stop_code;
	}

	public String getNumber_of_copies() {
		return number_of_copies;
	}

	public void setNumber_of_copies(String number_of_copies) {
		this.number_of_copies = number_of_copies;
	}

	public String getPending_expiration_date() {
		return pending_expiration_date;
	}

	public void setPending_expiration_date(String pending_expiration_date) {
		this.pending_expiration_date = pending_expiration_date;
	}

	public String getPromo_code() {
		return promo_code;
	}

	public void setPromo_code(String promo_code) {
		this.promo_code = promo_code;
	}

	public String getPub_code() {
		return pub_code;
	}

	public void setPub_code(String pub_code) {
		this.pub_code = pub_code;
	}

	public String getSource_code() {
		return source_code;
	}

	public void setSource_code(String source_code) {
		this.source_code = source_code;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getSubscriber_status() {
		return subscriber_status;
	}

	public void setSubscriber_status(String subscriber_status) {
		this.subscriber_status = subscriber_status;
	}

	public String getVacation_flag() {
		return vacation_flag;
	}

	public void setVacation_flag(String vacation_flag) {
		this.vacation_flag = vacation_flag;
	}

	public String getWork_phone() {
		return work_phone;
	}

	public void setWork_phone(String work_phone) {
		this.work_phone = work_phone;
	}

	public Errors getErrors() {
		return errors;
	}

	public void setErrors(Errors errors) {
		this.errors = errors;
	}

	@Override
	public boolean containsErrors() {
		boolean containsErrors = false;
		if (this.errors != null) {
			if (errors.getErrors() != null) {
				if (errors.getErrors().size() > 0) {
					containsErrors = true;
				}
			}
		} else { // only check the raw response if errors is null, this indicates a serious problem
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				containsErrors = true;
			}
		}
		return containsErrors;
	}

	@Override
	public Collection<String> getErrorMessages() {
		Collection<String> messages = new ArrayList<String>();
		if (this.errors != null) {
			if (errors.getErrors() != null) {
				if (errors.getErrors().size() > 0) {
					for (Error e : errors.getErrors()) {
						String aMessage = e.getValue();
						messages.add(aMessage);
					}
				}
			}
		} else { // only check the raw response if errors is null, this indicates a serious problem
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				messages.add(this.getRawResponse());
			}
		}
		return messages;
	}

	public String getFod_code() {
		return fod_code;
	}

	public void setFod_code(String fod_code) {
		this.fod_code = fod_code;
	}

}
