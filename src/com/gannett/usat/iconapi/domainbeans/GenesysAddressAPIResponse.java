package com.gannett.usat.iconapi.domainbeans;

import java.io.Serializable;

public abstract class GenesysAddressAPIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1604147227842316449L;

	private transient String rawResponse = null;

	public String getRawResponse() {
		return rawResponse;
	}

	public void setRawResponse(String rawResponse) {
		this.rawResponse = rawResponse;
	}
}
