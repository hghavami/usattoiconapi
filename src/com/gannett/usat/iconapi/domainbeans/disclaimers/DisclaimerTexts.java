package com.gannett.usat.iconapi.domainbeans.disclaimers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.iconapi.domainbeans.Error;
import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.GenesysBaseAPIResponse;

/**
 * @author hghavami
 * This class get the disclaimer text from the disclaimer ICON API 
 */
public class DisclaimerTexts extends GenesysBaseAPIResponse implements Serializable{

	private static final long serialVersionUID = -7790167641488681313L;
	private String gci_unit = null;

	private String market_pub_code = null;

	private String pub_code = null;
	private String fod_code = null;
	private String subscription_start_date = null;
	private String promotion_code = null;
	private String rate_code = null;
	private String delivery_method = null;
	private String ez_pay = null;
	private String disclaimer_text = null;
	private Errors errors = null;
	public DisclaimerTexts() {
		super();
	}
	
	public boolean containsErrors() {
		boolean containsErrors = false;
		if (this.errors != null) {
			if (errors.getErrors() != null) {
				if (errors.getErrors().size() > 0) {
					containsErrors = true;
				}
			}
		} else { // only check the raw response if errors is null, this indicates a serious problem
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				containsErrors = true;
			}
		}
		return containsErrors;
	}

	public String getDelivery_method() {
		return delivery_method;
	}

	public String getDisclaimer_text() {
		return disclaimer_text;
	}

	public Collection<String> getErrorMessages() {
		Collection<String> messages = new ArrayList<String>();
		if (this.errors != null) {
			if (errors.getErrors() != null) {
				if (errors.getErrors().size() > 0) {
					for (Error e : errors.getErrors()) {
						String aMessage = e.getValue();
						messages.add(aMessage);
					}
				}
			}
		} else { // only check the raw response if errors is null, this indicates a serious problem
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				messages.add(this.getRawResponse());
			}
		}
		return messages;
	}

	public Errors getErrors() {
		return errors;
	}

	public String getEz_pay() {
		return ez_pay;
	}

	public String getFod_code() {
		return fod_code;
	}

	public String getGci_unit() {
		return gci_unit;
	}

	public String getMarket_pub_code() {
		return market_pub_code;
	}

	public String getPromotion_code() {
		return promotion_code;
	}

	public String getPub_code() {
		return pub_code;
	}

	public String getRate_code() {
		return rate_code;
	}

	public String getSubscription_start_date() {
		return subscription_start_date;
	}

	public void setDelivery_method(String delivery_method) {
		this.delivery_method = delivery_method;
	}

	public void setDisclaimer_text(String disclaimer_text) {
		this.disclaimer_text = disclaimer_text;
	}

	public void setErrors(Errors errors) {
		this.errors = errors;
	}

	public void setEz_pay(String ez_pay_flag) {
		this.ez_pay = ez_pay_flag;
	}

	public void setFod_code(String fod_code) {
		this.fod_code = fod_code;
	}

	public void setGci_unit(String gci_unit) {
		this.gci_unit = gci_unit;
	}

	public void setMarket_pub_code(String market_pub_code) {
		this.market_pub_code = market_pub_code;
	}

	public void setPromotion_code(String promotion_code) {
		this.promotion_code = promotion_code;
	}

	public void setPub_code(String pub_code) {
		this.pub_code = pub_code;
	}

	public void setRate_code(String rate_code) {
		this.rate_code = rate_code;
	}

	public void setSubscription_start_date(String start_date) {
		this.subscription_start_date = start_date;
	}

}
