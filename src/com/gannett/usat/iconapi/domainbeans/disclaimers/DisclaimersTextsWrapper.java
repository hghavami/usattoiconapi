/**
 * 
 */
package com.gannett.usat.iconapi.domainbeans.disclaimers;

import java.io.Serializable;

import com.gannett.usat.iconapi.domainbeans.GenesysAddressAPIResponse;

/**
 * @author hghavami
 * This class wraps the Disclaimers JSON
 */
public class DisclaimersTextsWrapper extends GenesysAddressAPIResponse implements Serializable{

	private static final long serialVersionUID = 6123535203170988737L;

	public DisclaimersTextsWrapper() {
		super();
		this.disclaimer = new DisclaimerTexts();
	}

	private DisclaimerTexts disclaimer = null;

	public DisclaimerTexts getDisclaimer() {
		return disclaimer;
	}

	public void setDisclaimer(DisclaimerTexts disclaimer) {
		this.disclaimer = disclaimer;
	}
}
