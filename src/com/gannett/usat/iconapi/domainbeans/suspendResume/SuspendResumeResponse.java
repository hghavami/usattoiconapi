package com.gannett.usat.iconapi.domainbeans.suspendResume;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.iconapi.domainbeans.Error;
import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.GenesysBaseAPIResponse;

public class SuspendResumeResponse extends GenesysBaseAPIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SuspendResumeRequest vacation_stop = null;

	public SuspendResumeRequest getVacation_stop() {
		return vacation_stop;
	}

	public void setVacation_stop(SuspendResumeRequest suspend_resume_request) {
		this.vacation_stop = suspend_resume_request;
	}

	@Override
	public boolean containsErrors() {
		boolean containsErrors = false;
		if (this.vacation_stop != null) {
			Errors errors = this.vacation_stop.getErrors();
			if (errors != null) {
				if (errors.getErrors() != null) {
					if (errors.getErrors().size() > 0) {
						containsErrors = true;
					}
				}
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				containsErrors = true;
			}
		}
		return containsErrors;
	}

	@Override
	public Collection<String> getErrorMessages() {
		Collection<String> messages = new ArrayList<String>();
		if (this.vacation_stop != null) {
			Errors errors = this.vacation_stop.getErrors();
			if (errors != null) {
				if (errors.getErrors() != null) {
					for (Error e : errors.getErrors()) {
						String aMessage = e.getValue();
						messages.add(aMessage);
					}
				}
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				messages.add(this.getRawResponse());
			}
		}
		return messages;
	}

}
