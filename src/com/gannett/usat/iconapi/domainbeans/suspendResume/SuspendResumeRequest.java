package com.gannett.usat.iconapi.domainbeans.suspendResume;

import java.io.Serializable;

import com.gannett.usat.iconapi.domainbeans.Errors;

public class SuspendResumeRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8332352321801491385L;
	private String gci_unit = null;
	private String pub_code = null;
	private Errors errors = null;
	private String phone = null;
	private String stop_date = null;
	private String start_date = null;
	private String vacation_option = null;
	private String delivery_days = null;
	private String tran_id = null;

	public String getTrnnbr() {
		return tran_id;
	}

	public void setTrnnbr(String trnnbr) {
		this.tran_id = trnnbr;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone_number) {
		this.phone = phone_number;
	}

	public String getVacation_option() {
		return vacation_option;
	}

	public void setVacation_option(String vacation_option) {
		this.vacation_option = vacation_option;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getStop_date() {
		return stop_date;
	}

	public void setStop_date(String stop_date) {
		this.stop_date = stop_date;
	}

	public String getGci_unit() {
		return gci_unit;
	}

	public void setGci_unit(String gci_unit) {
		this.gci_unit = gci_unit;
	}

	public String getPub_code() {
		return pub_code;
	}

	public void setPub_code(String pub_code) {
		this.pub_code = pub_code;
	}

	public Errors getErrors() {
		return errors;
	}

	public void setErrors(Errors errors) {
		this.errors = errors;
	}

	public String getDelivery_days() {
		return delivery_days;
	}

	public void setDelivery_days(String delivery_days) {
		this.delivery_days = delivery_days;
	}
}
