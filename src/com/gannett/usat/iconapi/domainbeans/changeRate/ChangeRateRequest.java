package com.gannett.usat.iconapi.domainbeans.changeRate;

import java.io.Serializable;

import com.gannett.usat.iconapi.domainbeans.Errors;

public class ChangeRateRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8332352321801491385L;
	private String gci_unit = null;
	private String pub_code = null;
	private Errors errors = null;
	private String phone_number = null;
	private String stop_date = null;
	private String start_date = null;
	private String vacation_option = null;
	private String trnnbr = null;

	public String getTrnnbr() {
		return trnnbr;
	}

	public void setTrnnbr(String trnnbr) {
		this.trnnbr = trnnbr;
	}

	public String getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}

	public String getVacation_option() {
		return vacation_option;
	}

	public void setVacation_option(String vacation_option) {
		this.vacation_option = vacation_option;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getStop_date() {
		return stop_date;
	}

	public void setStop_date(String stop_date) {
		this.stop_date = stop_date;
	}

	public String getGci_unit() {
		return gci_unit;
	}

	public void setGci_unit(String gci_unit) {
		this.gci_unit = gci_unit;
	}

	public String getPub_code() {
		return pub_code;
	}

	public void setPub_code(String pub_code) {
		this.pub_code = pub_code;
	}

	public Errors getErrors() {
		return errors;
	}

	public void setErrors(Errors errors) {
		this.errors = errors;
	}
}
