package com.gannett.usat.iconapi.domainbeans.changeRate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.iconapi.domainbeans.Error;
import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.GenesysBaseAPIResponse;

public class ChangeRateResponse extends GenesysBaseAPIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ChangeRateRequest change_rate_request = null;

	public ChangeRateRequest getChange_rate_request() {
		return change_rate_request;
	}

	public void setChange_rate_request(ChangeRateRequest change_rate_request) {
		this.change_rate_request = change_rate_request;
	}

	@Override
	public boolean containsErrors() {
		boolean containsErrors = false;
		if (this.change_rate_request != null) {
			Errors errors = this.change_rate_request.getErrors();
			if (errors != null) {
				if (errors.getErrors() != null) {
					if (errors.getErrors().size() > 0) {
						containsErrors = true;
					}
				}
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				containsErrors = true;
			}
		}
		return containsErrors;
	}

	@Override
	public Collection<String> getErrorMessages() {
		Collection<String> messages = new ArrayList<String>();
		if (this.change_rate_request != null) {
			Errors errors = this.change_rate_request.getErrors();
			if (errors != null) {
				if (errors.getErrors() != null) {
					for (Error e : errors.getErrors()) {
						String aMessage = e.getValue();
						messages.add(aMessage);
					}
				}
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				messages.add(this.getRawResponse());
			}
		}
		return messages;
	}

}
