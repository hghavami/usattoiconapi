package com.gannett.usat.iconapi.domainbeans.updateEmail;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.iconapi.domainbeans.Error;
import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.GenesysBaseAPIResponse;

public class UpdateEmailResponse extends GenesysBaseAPIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UpdateEmailRequest change_of_subscription = null;

	public UpdateEmailRequest getSuspend_resume_request() {
		return change_of_subscription;
	}

	public void setSuspend_resume_request(UpdateEmailRequest update_email_request) {
		this.change_of_subscription = update_email_request;
	}

	@Override
	public boolean containsErrors() {
		boolean containsErrors = false;
		if (this.change_of_subscription != null) {
			Errors errors = this.change_of_subscription.getErrors();
			if (errors != null) {
				if (errors.getErrors() != null) {
					if (errors.getErrors().size() > 0) {
						containsErrors = true;
					}
				}
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				containsErrors = true;
			}
		}
		return containsErrors;
	}

	@Override
	public Collection<String> getErrorMessages() {
		Collection<String> messages = new ArrayList<String>();
		if (this.change_of_subscription != null) {
			Errors errors = this.change_of_subscription.getErrors();
			if (errors != null) {
				if (errors.getErrors() != null) {
					for (Error e : errors.getErrors()) {
						String aMessage = e.getValue();
						messages.add(aMessage);
					}
				}
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				messages.add(this.getRawResponse());
			}
		}
		return messages;
	}

}
