package com.gannett.usat.iconapi.domainbeans.changeCard;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.iconapi.domainbeans.Error;
import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.GenesysBaseAPIResponse;

public class ChangeCardEzPayResponse extends GenesysBaseAPIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ChangeCardRequest signup_ezpay = null;

	public ChangeCardRequest getSignup_ezpay() {
		return signup_ezpay;
	}

	public void setSignup_ezpay(ChangeCardRequest change_card_request) {
		this.signup_ezpay = change_card_request;
	}

	@Override
	public boolean containsErrors() {
		boolean containsErrors = false;
		if (this.signup_ezpay != null) {
			Errors errors = this.signup_ezpay.getErrors();
			if (errors != null) {
				if (errors.getErrors() != null) {
					if (errors.getErrors().size() > 0) {
						containsErrors = true;
					}
				}
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				containsErrors = true;
			}
		}
		return containsErrors;
	}

	@Override
	public Collection<String> getErrorMessages() {
		Collection<String> messages = new ArrayList<String>();
		if (this.signup_ezpay != null) {
			Errors errors = this.signup_ezpay.getErrors();
			if (errors != null) {
				if (errors.getErrors() != null) {
					for (Error e : errors.getErrors()) {
						String aMessage = e.getValue();
						messages.add(aMessage);
					}
				}
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				messages.add(this.getRawResponse());
			}
		}
		return messages;
	}

}
