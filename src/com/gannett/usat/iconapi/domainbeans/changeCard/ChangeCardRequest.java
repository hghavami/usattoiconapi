package com.gannett.usat.iconapi.domainbeans.changeCard;

import java.io.Serializable;

import com.gannett.usat.iconapi.domainbeans.Errors;

public class ChangeCardRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8332352321801491385L;
	private String gci_unit = null;
	private String pub_code = null;
	private Errors errors = null;
	private String tran_id = null;

	public String getTran_id() {
		return tran_id;
	}

	public void setTran_id(String trnnbr) {
		this.tran_id = trnnbr;
	}

	public String getGci_unit() {
		return gci_unit;
	}

	public void setGci_unit(String gci_unit) {
		this.gci_unit = gci_unit;
	}

	public String getPub_code() {
		return pub_code;
	}

	public void setPub_code(String pub_code) {
		this.pub_code = pub_code;
	}

	public Errors getErrors() {
		return errors;
	}

	public void setErrors(Errors errors) {
		this.errors = errors;
	}
}
