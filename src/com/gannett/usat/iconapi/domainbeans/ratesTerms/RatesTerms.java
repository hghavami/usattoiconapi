package com.gannett.usat.iconapi.domainbeans.ratesTerms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.iconapi.domainbeans.Error;
import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.GenesysBaseAPIResponse;

public class RatesTerms extends GenesysBaseAPIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5088138208668592631L;

	private String gci_unit = null;
	private String account_number = null;
	private String contest_code = null;
	private String fod_code = null;
	private String pia_rate_code = null;
	private String rate_period = null;
	private String renewal_rate_code = null;
	private String renewal_period_term_length = null;
	private String ez_pay_required_flag = null;
	private String amount = null;
	private String promo_code = null;

	private String pub_code = null;
	private String source_code = null;
	private String term_length = null;
	private String description = null;
	private String renewal = null;
	private Errors errors = null;

	public RatesTerms() {
		super();
	}

	public String getRenewalPeriodTermLength() {
		return renewal_period_term_length;
	}

	public void setRenewalPeriodTermLength(String renewal_period_term_length) {
		this.renewal_period_term_length = renewal_period_term_length;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getEzpayRequired() {
		return ez_pay_required_flag;
	}

	public void setEzpayRequired(String ez_pay_required_flag) {
		this.ez_pay_required_flag = ez_pay_required_flag;
	}

	public String getRenewalRateCode() {
		return renewal_rate_code;
	}

	public void setRenewalRateCode(String renewal_rate_code) {
		this.renewal_rate_code = renewal_rate_code;
	}

	public String getRatePeriod() {
		return rate_period;
	}

	public void setRatePeriod(String rate_period) {
		this.rate_period = rate_period;
	}

	public String getPiaRateCode() {
		return pia_rate_code;
	}

	public void setPiaRateCode(String pia_rate_code) {
		this.pia_rate_code = pia_rate_code;
	}

	public String getGci_unit() {
		return gci_unit;
	}

	public void setGci_unit(String gci_unit) {
		this.gci_unit = gci_unit;
	}

	public String getRenewal() {
		return renewal;
	}

	public void setRenewal(String renewal) {
		this.renewal = renewal;
	}

	public String getAccount_number() {
		return account_number;
	}

	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTerm_length() {
		return term_length;
	}

	public void setTerm_length(String term_length) {
		this.term_length = term_length;
	}

	public String getContest_code() {
		return contest_code;
	}

	public void setContest_code(String contest_code) {
		this.contest_code = contest_code;
	}

	public String getPromo_code() {
		return promo_code;
	}

	public void setPromo_code(String promo_code) {
		this.promo_code = promo_code;
	}

	public String getPub_code() {
		return pub_code;
	}

	public void setPub_code(String pub_code) {
		this.pub_code = pub_code;
	}

	public String getSource_code() {
		return source_code;
	}

	public void setSource_code(String source_code) {
		this.source_code = source_code;
	}

	public Errors getErrors() {
		return errors;
	}

	public void setErrors(Errors errors) {
		this.errors = errors;
	}

	@Override
	public boolean containsErrors() {
		boolean containsErrors = false;
		if (this.errors != null) {
			if (errors.getErrors() != null) {
				if (errors.getErrors().size() > 0) {
					containsErrors = true;
				}
			}
		} else { // only check the raw response if errors is null, this indicates a serious problem
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				containsErrors = true;
			}
		}
		return containsErrors;
	}

	@Override
	public Collection<String> getErrorMessages() {
		Collection<String> messages = new ArrayList<String>();
		if (this.errors != null) {
			if (errors.getErrors() != null) {
				if (errors.getErrors().size() > 0) {
					for (Error e : errors.getErrors()) {
						String aMessage = e.getValue();
						messages.add(aMessage);
					}
				}
			}
		} else { // only check the raw response if errors is null, this indicates a serious problem
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				messages.add(this.getRawResponse());
			}
		}
		return messages;
	}

	public String getFod() {
		return fod_code;
	}

	public void setFod(String fod_code) {
		this.fod_code = fod_code;
	}

}
