package com.gannett.usat.iconapi.domainbeans.DeliveryMethod;

import java.io.Serializable;

import com.gannett.usat.iconapi.domainbeans.GenesysAddressAPIResponse;
import com.gannett.usat.iconapi.domainbeans.Code1Validation.Code1Validation;

public class DeliveryMethodCode1ValidationWrapper extends GenesysAddressAPIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5088138208668592631L;

	public DeliveryMethodCode1ValidationWrapper() {
		super();
		this.address = new Code1Validation();
		this.route = new DeliveryMethod();
	}

	private DeliveryMethod route = null;
	private Code1Validation address = null;

	public DeliveryMethod getRoute() {
		return route;
	}

	public void setRoute(DeliveryMethod deliveryMethod) {
		this.route = deliveryMethod;
	}

	public Code1Validation getAddress() {
		return address;
	}

	public void setAddress(Code1Validation code1Validation) {
		this.address = code1Validation;
	}

}
