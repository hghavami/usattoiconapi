package com.gannett.usat.iconapi.domainbeans.DeliveryMethod;

import java.io.Serializable;

import com.gannett.usat.iconapi.domainbeans.GenesysAddressAPIResponse;

public class DeliveryMethod extends GenesysAddressAPIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5088138208668592631L;

	private String pub_out = null;
	private String zone_out = null;
	private String dist_out = null;
	private String route_out = null;
	private String delmeth_out = null;
	private String source_out = null;
	private String errorcode = null;
	private String route_stat = null;
	private String delivery_method_return = null;

	public DeliveryMethod() {
		super();
	}

	public String getPub_out() {
		return pub_out;
	}

	public void setPub_out(String last_payment_date) {
		this.pub_out = last_payment_date;
	}

	public String getZone_out() {
		return zone_out;
	}

	public void setZone_out(String last_stop_code) {
		this.zone_out = last_stop_code;
	}

	public String getDist_out() {
		return dist_out;
	}

	public void setDist_out(String number_of_copies) {
		this.dist_out = number_of_copies;
	}

	public String getRoute_out() {
		return route_out;
	}

	public void setRoute_out(String pending_expiration_date) {
		this.route_out = pending_expiration_date;
	}

	public String getDelmeth_out() {
		return delmeth_out;
	}

	public void setDelmeth_out(String promo_code) {
		this.delmeth_out = promo_code;
	}

	public String getSource_out() {
		return source_out;
	}

	public void setSource_out(String pub_code) {
		this.source_out = pub_code;
	}

	public String getRoute_stat() {
		return route_stat;
	}

	public void setRoute_stat(String start_date) {
		this.route_stat = start_date;
	}

	public String getDelivery_method_return() {
		return delivery_method_return;
	}

	public void setDelivery_method_return(String subscriber_status) {
		this.delivery_method_return = subscriber_status;
	}

	public String getErrorcode() {
		return errorcode;
	}

	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}

}
