package com.gannett.usat.iconapi.domainbeans.newSubscriptionStart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.iconapi.domainbeans.Error;
import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.GenesysBaseAPIResponse;

public class StartSubscriptionResponse extends GenesysBaseAPIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 378663911177617341L;
	private StartSubscription start_subscription = null;

	@Override
	public boolean containsErrors() {
		boolean containsErrors = false;
		if (this.start_subscription != null) {
			Errors errors = this.start_subscription.getErrors();
			if (errors != null) {
				if (errors.getErrors() != null) {
					if (errors.getErrors().size() > 0) {
						containsErrors = true;
					}
				}
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				containsErrors = true;
			}
		}
		return containsErrors;
	}

	@Override
	public Collection<String> getErrorMessages() {
		Collection<String> messages = new ArrayList<String>();
		if (this.start_subscription != null) {
			Errors errors = this.start_subscription.getErrors();
			if (errors != null) {
				if (errors.getErrors() != null) {
					for (Error e : errors.getErrors()) {
						String aMessage = e.getValue();
						messages.add(aMessage);
					}
				}
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				messages.add(this.getRawResponse());
			}
		}
		return messages;
	}

	public StartSubscription getStart_subscription() {
		return start_subscription;
	}

	public void setStart_subscription(StartSubscription start_subscription) {
		this.start_subscription = start_subscription;
	}

}
