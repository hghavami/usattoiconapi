package com.gannett.usat.iconapi.domainbeans.newSubscriptionStart;

import java.io.Serializable;

import com.gannett.usat.iconapi.domainbeans.Errors;

public class StartSubscription implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8268766423685514995L;

	private String subscription_tax = null;
	private String subscription_total = null;
	private String subscription_subtotal = null;
	private String account_number = null;
	private String transaction_number = null;
	private String pub_code = null;
	private String gci_unit = null;
	private Errors errors = null;

	public Errors getErrors() {
		return errors;
	}

	public void setErrors(Errors errors) {
		this.errors = errors;
	}

	public String getSubscription_tax() {
		return subscription_tax;
	}

	public void setSubscription_tax(String subscription_tax) {
		this.subscription_tax = subscription_tax;
	}

	public String getSubscription_total() {
		return subscription_total;
	}

	public void setSubscription_total(String subscription_total) {
		this.subscription_total = subscription_total;
	}

	public String getSubscription_subtotal() {
		return subscription_subtotal;
	}

	public void setSubscription_subtotal(String subscription_subtotal) {
		this.subscription_subtotal = subscription_subtotal;
	}

	public String getAccount_number() {
		return account_number;
	}

	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}

	public String getTransaction_number() {
		return transaction_number;
	}

	public void setTransaction_number(String transaction_number) {
		this.transaction_number = transaction_number;
	}

	public String getPub_code() {
		return pub_code;
	}

	public void setPub_code(String pub_code) {
		this.pub_code = pub_code;
	}

	public String getGci_unit() {
		return gci_unit;
	}

	public void setGci_unit(String gci_unit) {
		this.gci_unit = gci_unit;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
