package com.gannett.usat.iconapi.domainbeans.deliveryIssue;

import java.io.Serializable;

import com.gannett.usat.iconapi.domainbeans.Errors;

public class DeliveryServiceIssue implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8332352321801491385L;

	private String redelivery_available = null;
	private String delivery_days = null;
	private String id = null;
	private String gci_unit = null;
	private String pub_code = null;
	private Errors errors = null;

	public String getRedelivery_available() {
		return redelivery_available;
	}

	public void setRedelivery_available(String redelivery_available) {
		this.redelivery_available = redelivery_available;
	}

	public String getDelivery_days() {
		return delivery_days;
	}

	public void setDelivery_days(String delivery_days) {
		this.delivery_days = delivery_days;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGci_unit() {
		return gci_unit;
	}

	public void setGci_unit(String gci_unit) {
		this.gci_unit = gci_unit;
	}

	public String getPub_code() {
		return pub_code;
	}

	public void setPub_code(String pub_code) {
		this.pub_code = pub_code;
	}

	public Errors getErrors() {
		return errors;
	}

	public void setErrors(Errors errors) {
		this.errors = errors;
	}
}
