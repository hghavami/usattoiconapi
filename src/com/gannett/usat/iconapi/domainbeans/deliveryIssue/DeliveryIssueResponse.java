package com.gannett.usat.iconapi.domainbeans.deliveryIssue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.iconapi.domainbeans.Error;
import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.GenesysBaseAPIResponse;

public class DeliveryIssueResponse extends GenesysBaseAPIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DeliveryServiceIssue delivery_service_issue = null;

	public DeliveryServiceIssue getDelivery_service_issue() {
		return delivery_service_issue;
	}

	public void setDelivery_service_issue(DeliveryServiceIssue delivery_service_issue) {
		this.delivery_service_issue = delivery_service_issue;
	}

	@Override
	public boolean containsErrors() {
		boolean containsErrors = false;
		if (this.delivery_service_issue != null) {
			Errors errors = this.delivery_service_issue.getErrors();
			if (errors != null) {
				if (errors.getErrors() != null) {
					if (errors.getErrors().size() > 0) {
						containsErrors = true;
					}
				}
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				containsErrors = true;
			}
		}
		return containsErrors;
	}

	@Override
	public Collection<String> getErrorMessages() {
		Collection<String> messages = new ArrayList<String>();
		if (this.delivery_service_issue != null) {
			Errors errors = this.delivery_service_issue.getErrors();
			if (errors != null) {
				if (errors.getErrors() != null) {
					for (Error e : errors.getErrors()) {
						String aMessage = e.getValue();
						messages.add(aMessage);
					}
				}
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				messages.add(this.getRawResponse());
			}
		}
		return messages;
	}

}
