package com.gannett.usat.iconapi.domainbeans.makePayment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.iconapi.domainbeans.Error;
import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.GenesysBaseAPIResponse;

public class MakeAPaymentResponse extends GenesysBaseAPIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2467545048925995294L;

	private MakeAPayment make_a_payment = null;

	@Override
	public boolean containsErrors() {
		boolean containsErrors = false;
		if (this.make_a_payment != null) {
			Errors errors = this.make_a_payment.getErrors();
			if (errors != null) {
				if (errors.getErrors() != null) {
					if (errors.getErrors().size() > 0) {
						containsErrors = true;
					}
				}
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				containsErrors = true;
			}
		}
		return containsErrors;
	}

	@Override
	public Collection<String> getErrorMessages() {
		Collection<String> messages = new ArrayList<String>();
		if (this.make_a_payment != null) {
			Errors errors = this.make_a_payment.getErrors();
			if (errors != null) {
				if (errors.getErrors() != null) {
					for (Error e : errors.getErrors()) {
						String aMessage = e.getValue();
						messages.add(aMessage);
					}
				}
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				messages.add(this.getRawResponse());
			}
		}
		return messages;
	}

	public MakeAPayment getMake_a_payment() {
		return make_a_payment;
	}

	public void setMake_a_payment(MakeAPayment make_a_payment) {
		this.make_a_payment = make_a_payment;
	}

}
