package com.gannett.usat.iconapi.domainbeans.makePayment;

import java.io.Serializable;

import com.gannett.usat.iconapi.domainbeans.Errors;

public class MakeAPayment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2038188257747657208L;

	private String gci_unit = null;
	private String account_number = null;
	private String pub_code = null;
	private Errors errors = null;
	private String card_number = null;
	private String card_type = null;
	private String card_exp = null;
	private String payment = null;
	private String enroll_ezpay = null;
	private String ratecode = null;
	private String termlength = null;
	private String tran_id = null;
	private String subscription_tax = null;
	private String subscription_total = null;
	private String subscription_subtotal = null;

	public String getTermlength() {
		return termlength;
	}

	public void settermlength(String termlength) {
		this.termlength = termlength;
	}

	public String getRatecode() {
		return ratecode;
	}

	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}

	public String enroll_ezpay() {
		return enroll_ezpay;
	}

	public void setEzpay(String enroll_ezpay) {
		this.enroll_ezpay = enroll_ezpay;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getCardexp() {
		return card_exp;
	}

	public void setCardexp(String card_exp) {
		this.card_exp = card_exp;
	}

	public String getCardtype() {
		return card_type;
	}

	public void setCardtype(String card_type) {
		this.card_type = card_type;
	}

	public String getCardnbr() {
		return card_number;
	}

	public void setCardnbr(String card_number) {
		this.card_number = card_number;
	}

	public String getAcctnbr() {
		return account_number;
	}

	public void setAcctnbr(String account_number) {
		this.account_number = account_number;
	}

	public String getGci_unit() {
		return gci_unit;
	}

	public void setGci_unit(String gci_unit) {
		this.gci_unit = gci_unit;
	}

	public String getPub_code() {
		return pub_code;
	}

	public void setPub_code(String pub_code) {
		this.pub_code = pub_code;
	}

	public String getTran_id() {
		return tran_id;
	}

	public void setTran_id(String tran_id) {
		this.tran_id = tran_id;
	}

	public Errors getErrors() {
		return errors;
	}

	public void setErrors(Errors errors) {
		this.errors = errors;
	}

	public String getSubscription_tax() {
		return subscription_tax;
	}

	public void setSubscription_tax(String subscription_tax) {
		this.subscription_tax = subscription_tax;
	}

	public String getSubscription_total() {
		return subscription_total;
	}

	public void setSubscription_total(String subscription_total) {
		this.subscription_total = subscription_total;
	}

	public String getSubscription_subtotal() {
		return subscription_subtotal;
	}

	public void setSubscription_subtotal(String subscription_subtotal) {
		this.subscription_subtotal = subscription_subtotal;
	}

}
